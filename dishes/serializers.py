from django.contrib.auth.models import User
from rest_framework import serializers

from djoser import constants, utils
from django.contrib.auth import authenticate, get_user_model

from dishes.models import Dish, CouriersOrder, CouriersOrderPart, DishInOrderForCafe, Order, Category, \
    CategoryName, CafesOrderPart, CouriersIngredientsTransit, CouriersIngredientsTransitIngredient
from locations.models import Location
from _auth.models import Profile
from locations.serializers import LocationSerializer

class DishesSerializer(serializers.ModelSerializer):
    categories = serializers.SerializerMethodField()

    def get_categories(self, dish):
        try:
            cats = Category.objects.filter(dish_id=dish.pk)
            return CategoryNameSerializer([cat.category for cat in cats], many=True).data
        except Exception: return None

    class Meta:
        model = Dish
        fields = ("id", "name", "image", "description", "calorie", "weight", "price", "categories")

class CategoryNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryName
        fields = ("id", "name")


class CouriersOrderPartSerializer(serializers.ModelSerializer):
    cafe = serializers.SerializerMethodField()

    def get_cafe(self, orderPart):
        try:
            return LocationSerializer(orderPart.cafe.location).data
        except Exception: return None


    class Meta:
        model = CouriersOrderPart
        fields = ("id", "cafe", "time", "query")


class OrderDetailsForCourierSerializer(serializers.ModelSerializer):
    order_parts = serializers.SerializerMethodField()
    time = serializers.SerializerMethodField()
    client = serializers.SerializerMethodField()

    def get_client(self, order):
        try:
            return LocationSerializer(order.order.location).data
        except Exception: return None

    def get_order_parts(self, order):
        try:
            orderParts = CouriersOrderPart.objects.filter(couriers_order_id=order.pk).order_by("time")
            return CouriersOrderPartSerializer(orderParts, many=True).data
        except Exception: return None

    class Meta:
        model = CouriersOrder
        fields = ("id", "order_parts", "client")


class OrderForCourierSerializer(serializers.ModelSerializer):
    client = serializers.SerializerMethodField()
    order_parts = serializers.SerializerMethodField()
    order = serializers.SerializerMethodField()

    def get_order(self, order):
        try:
            return {"id": order.order.pk, "time": order.order.datetime}
        except Exception: return None

    def get_client(self, order):
        try:
            return LocationSerializer(order.order.location).data
        except Exception: return None

    def get_order_parts(self, order):
        try:
            orderParts = CouriersOrderPart.objects.filter(couriers_order_id=order.pk).order_by("time")
            return CouriersOrderPartSerializer(orderParts, many=True).data
        except Exception: return None

    class Meta:
        model = CouriersOrder
        fields = ("id", "order", "client", "total_cost", "order_parts")


class CouriersIngredientsTransitIngredientForCourSerializer(serializers.ModelSerializer):
    from_cafe = serializers.SerializerMethodField()

    def get_from_cafe(self, ingrTransitIngr):
        try:
            return LocationSerializer(ingrTransitIngr.from_cafe.location).data
        except Exception: return None

    class Meta:
        model = CouriersIngredientsTransitIngredient
        fields = ("id", "from_cafe", "queue")


class CouriersIngredientsTransitSerializer(serializers.ModelSerializer):
    to_cafe = serializers.SerializerMethodField()
    transit_parts = serializers.SerializerMethodField()

    def get_transit_parts(self, transit):
        try:
            parts = CouriersIngredientsTransitIngredient.objects.filter(ingr_import_id=transit.pk)
            return CouriersIngredientsTransitIngredientForCourSerializer(parts, many=True).data
        except Exception: return None

    def get_to_cafe(self, transit):
        try:
            return LocationSerializer(transit.to_cafe.location).data
        except Exception:
            return None


    class Meta:
        model = CouriersIngredientsTransit
        fields = ("id", "to_cafe", "transit_parts")


class CafeNewOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = CafesOrderPart
        fields = ("id", "cafe")
