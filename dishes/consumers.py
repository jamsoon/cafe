import asyncio
import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
import urllib
from time import sleep

from .models import *
from _auth.models import Profile
from dishes.serializers import DishesSerializer
from locations.models import *

class newOrder(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connected", event)
        print(self.scope)

        try:
            self.user = self.scope["user"]
            self.user_profile = await self.get_user_role(self.user)
            self.user_role = self.user_profile.role
            self.couriers_group = "couriers"

            if self.user_role == "courier":
                await self.channel_layer.group_add(
                    self.couriers_group,
                    self.channel_name
                )
                await self.send({"type": "websocket.accept"})
                # await self.send({"type": "websocket.send",
                #                  "text": json.dumps({
                #                      "message": "МЕНЮ ОБРАБОТКИ ЗАКАЗОВ КУРЬЕРОМ. ДОБРО ПОЖАЛОВАТЬ, [%s]" % self.user.username,
                #                      "type": "new_order",
                #                  })
                #                 })

            elif self.user_role == "manager" and self.user_profile.worksInCafe:
                print("MANAGER")
                cafe = self.user_profile.worksInCafe
                cafeID = cafe.id
                await self.channel_layer.group_add(
                    "cafe" + str(cafeID),
                    self.channel_name
                )
                await self.send({"type": "websocket.accept"})
                print("ACCEPTED ")
                # await self.send({"type": "websocket.send",
                #                  "text": json.dumps({
                #                      "type": "new_order",
                #                      "message": "МЕНЮ ОБРАБОТКИ ЗАКАЗОВ В КАФЕ [%s]. ДОБРО ПОЖАЛОВАТЬ, [%s]" % (cafe.location.street_name, self.user.username),
                #                  })
                #                 })
        except Exception: pass


    async def websocket_receive(self, event):
        print("receive", event)

    async def websocket_disconnect(self, event):
        print("disconnected", event)


    async def sendNewOrderCourier(self, event):
        await self.send({
            "type": "websocket.send",
            "text": json.dumps({
                "order": event["order"],
            })
        })

    async def sendNewOrderCafe(self, event):
        await self.send({
            "type": "websocket.send",
            "text": event["text"]
        })

    @database_sync_to_async
    def get_user_role(self, user):
        return Profile.objects.get(user=user)






class CourierLocation(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connected", event)
        # for k, v in self.scope.items():
        #     print(k, ":", v)

        self.user = self.scope["user"]
        self.user_profile = await self.get_user_profile(self.user)
        self.user_role = self.user_profile.role

        print("self.scope:", self.scope)

        self.orderID = urllib.parse.parse_qs(self.scope["query_string"]).get(b"id")[0].decode("utf-8")

        try:
            order = Order.objects.get(id=self.orderID)
        except Exception:
            pass
        else:
            self.user_group = "user%s" % self.orderID
            self.courier_group = "courier%s" % self.orderID

            self.as_user = True if order.user_id == self.user.pk else False

            if self.as_user:
                print("CONNECTED TO %s GROUP" % self.user_group)
                await self.channel_layer.group_add(
                    self.user_group,
                    self.channel_name
                )
                await self.send({
                    "type": "websocket.accept"
                })


                # Отправка последних координат курьеров
                couriersIDs_ = CouriersOrder.objects.filter(order_id=self.orderID).values_list("courier_id", flat=True)
                couriersIDs = []

                for courierID in couriersIDs_:
                    if courierID is not None:
                        couriersIDs.append(courierID)

                if couriersIDs:
                    lastPositions = LastPosition.objects.filter(user_id__in=couriersIDs)

                    for lp in lastPositions:
                        print("lp", lp)
                        await self.sendLocation({
                            "courier": lp.pk,
                            "lat": lp.lat,
                            "lon": lp.lon,
                        })



                print("couriersIDs", couriersIDs)



                # await self.send({"type": "websocket.send",
                #                   "text": json.dumps({
                #                       "message": ""
                #                   })
                #                 })



    async def websocket_receive(self, event):
        pass

    async def websocket_disconnect(self, event):
        print("disconnected", event)

    async def sendLocation(self, event):
        print(event)
        await self.send({
            "type": "websocket.send",
            "text": json.dumps({
                "courier": event["courier"],
                "lon": event["lon"],
                "lat": event["lat"],
            })
        })

    @database_sync_to_async
    def get_user_profile(self, user):
        return Profile.objects.get(user=user)

