from django.db import models
from django.utils.deconstruct import deconstructible
#from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
import os
import sys
from cafe import settings
import string
import random
from django.utils.timezone import now as time_now

from PIL import Image
from io import BytesIO
from django.core.files.base import ContentFile
from resizeimage import resizeimage
from django.core.validators import FileExtensionValidator

from ingredients.models import Units
from cafes.models import Cafe

from ingredients.models import IngredientName, Ingredient
from locations.models import Location

# def get_sentinel_user():
#     return get_user_model().objects.get_or_create(username = 'Удалённый пользователь')[0]

# @deconstructible
# class Rename(object):
#     '''
#     Генерирует название файла, возвращает полный путь к файлу
#     '''
#     def __init__(self, sub_path):
#         self.path = sub_path
#
#     def __call__(self, instance, filename):
#         print('RENAME CALLED')
#         new_path = ''
#         fullpath = ''
#         while not fullpath or os.path.isfile(fullpath):
#             print(fullpath)
#             ext = filename.split(".")[-1]
#             random_name = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(12)])
#             new_filename = random_name + ".%s" % ext
#             new_path = os.path.join(self.path, new_filename)
#             fullpath = os.path.join(settings.MEDIA_ROOT, new_path)
#
#         return new_path


def Rename(filename, path):
    print('RENAME CALLED')
    random_name = ''
    fullpath = ''
    ext = filename.split(".")[-1]

    while not fullpath or os.path.isfile(fullpath):
        print(fullpath)
        random_name = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(12)])
        new_filename = random_name + ".%s" % ext
        new_path = os.path.join(path, new_filename)
        fullpath = os.path.join(settings.MEDIA_ROOT, new_path)

    return (random_name, ext)



class CategoryName(models.Model):
    name = models.CharField("Назва", unique=True, max_length=40)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Назва категорії"
        verbose_name_plural = "Назви категорій"
        ordering = ('name',)

class Category(models.Model):
    category = models.ForeignKey("CategoryName", models.CASCADE)
    dish = models.ForeignKey("Dish", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Категорія"
        verbose_name_plural = "Категорії"
        ordering = ()


class Dish(models.Model):
    name  = models.CharField("Назва", unique=True, max_length=150)
    image = models.ImageField("Зображення", upload_to="added/dishes/", default="added/dishes/default.jpg", validators=[FileExtensionValidator(allowed_extensions=['jpg', 'jpeg', 'png'])])
    description = models.TextField("Опис")
    calorie = models.FloatField("Калорійність (кал)", default=1.0)
    weight  = models.FloatField("Маса (г)", default=1.0)
    price = models.FloatField("Цiна", default=0, blank=True)
    cooking_time = models.IntegerField("Час приготування", default=1, blank=False, null=False)
    prepared_lifetime = models.IntegerField("Термін придатності заготовленої страви (хв)", null=True, default=60)
    prepared_cooking_time = models.IntegerField("Час приготування заготовленої страви", null=True, default=None)
    is_active = models.BooleanField("Чи активне", default=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        print("SAVE STARTED")
        from time import sleep

        try:
            old_image = Dish.objects.filter(id=self.id)[0].image
        except Exception:
            old_image = None
        pil_image_obj = Image.open(self.image)
        try:
            new_image = resizeimage.resize_width(pil_image_obj, 1920)
        except: # если ширина картинки меньше необходимой
            new_image = pil_image_obj

        temp_name = self.image.name
        format = "JPEG" if temp_name.split('.')[-1].strip() in ("jpg", "jpeg") else "PNG"

        new_image_io = BytesIO()
        new_image = new_image.convert("RGB")
        new_image.save(new_image_io, format=format)

        # new_image_min_io = BytesIO()
        # new_image_min = new_image_min.convert("RGB")
        # new_image_min.save(new_image_min_io, format=format)

        if old_image:
            full_old_img_path = os.path.join(settings.MEDIA_ROOT, old_image.name)

            if os.path.isfile(full_old_img_path):
                # удаление основного изображения
                try:
                    os.remove(full_old_img_path)
                except Exception:
                    pass
                # удаление уменьшенного изображения
                # try:
                #     old_image_path_parts = old_image.name.split('.')
                #     os.remove(old_image_path_parts[:-1] + '.min.' + old_image_path_parts[-1])
                # except Exception:
                #     pass

        self.image.delete(save=False)

        # self.image.save(
        #     name=temp_name + 'asd',
        #     content=ContentFile(new_image_io.getvalue()),
        #     save=False
        # )
        newNameTuple = Rename(temp_name, "added/dishes/")
        newName = newNameTuple[0]
        fileExt = newNameTuple[1]
        print('new_name', newName)
        print("SAVING...")
        self.image.save(
            name="%s.%s" % (newName, fileExt),
            content=ContentFile(new_image_io.getvalue()),
            save=False
        )
        print("SAVED")

        super(Dish, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Страва"
        verbose_name_plural = "Список страв"
        ordering = ('name',)


class DishIngredients(models.Model):
    dish = models.ForeignKey(Dish, verbose_name="Страва", on_delete=models.CASCADE)
    ingredient = models.ForeignKey(IngredientName, verbose_name="Iнгредієнт", on_delete=models.CASCADE)
    amount = models.IntegerField("К-ть", blank=True, default=1)

    def __str__(self):
        return self.dish.name

    def save(self, *args, **kwargs):
        if self.amount == None:
            self.amount = 1

        super(DishIngredients, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Інгредієнт"
        verbose_name_plural = "Інгредієнти"
        unique_together = ("dish", "ingredient")

class DishInCart(models.Model):
    dish = models.ForeignKey("Dish", verbose_name="Страва", on_delete=models.CASCADE, null=True)
    qty  = models.IntegerField("Кiлькiсть", default=1)
    user = models.ForeignKey(User, verbose_name="Користувач", on_delete=models.CASCADE, blank=True, null=True)
    user_cookie = models.CharField("User Hash", max_length=20, blank=True, null=True)

    def __str__(self):
        try:
            return self.dish.name
        except:
            return ""

    class Meta:
        verbose_name = "Cтрава в кошику"
        verbose_name_plural = "Список страв в кошиках"
        ordering = ('dish',)
        unique_together = ("dish", "user", "user_cookie")


class Order(models.Model):
    STATUSES = (
        ("in_process", "В обробці"),
        ("done", "Виконано")
    )
    user = models.ForeignKey(User, verbose_name="Замовник", on_delete=models.CASCADE)
    location = models.ForeignKey(Location, verbose_name="Локація", null=True, default=None, on_delete=models.PROTECT)
    datetime = models.DateTimeField("Дата замовлення", default=time_now)
    total_cost = models.FloatField("Загальна вартість", default=1, blank=True, null=True)
    status = models.CharField("Статус", choices=STATUSES, max_length=len(STATUSES[0][0]), null=True, default=STATUSES[0][0], blank=True)

    def __str__(self):
        return "Замовлення #%d" % self.id

    class Meta:
        verbose_name = "Замовлення"
        verbose_name_plural = "Список замовлень"
        ordering = ("datetime", "user", "total_cost")

class DishInOrder(models.Model):
    order = models.ForeignKey("Order", on_delete=models.CASCADE)
    dish  = models.ForeignKey("Dish", verbose_name="Страва", on_delete=models.SET(None), null=True)
    qty   = models.IntegerField("Кiлькiсть", default=1)

    def __str__(self):
        return "Об'єкт %s" % self.order.__str__

    class Meta:
        verbose_name = "Cтрава в замовленнi"
        verbose_name_plural = "Список страв в замовленнях"
        ordering = ('dish',)
        unique_together = ("order", "dish",)

class PreparedDish(models.Model):
    dish  = models.ForeignKey("Dish", verbose_name="Страва", on_delete=models.CASCADE, null=True)
    cafe = models.ForeignKey(Cafe, verbose_name="Кафе", on_delete=models.PROTECT, null=True)
    qty = models.IntegerField("Кiлькiсть", default=1)
    add_time = models.DateTimeField("Додано", null=True, editable=False, default=time_now)

    def __str__(self):
        try:
            return "Заготовлена страва %s" % self.dish.name
        except Exception:
            return "Заготовлена страва %d" % self.id

    class Meta:
        verbose_name = "Заготовлена страва"
        verbose_name_plural = "Заготовленi страви"


class PreparedDishesUsed(models.Model):
    ordered_dish = models.ForeignKey("DishInOrderForCafe", on_delete=models.CASCADE)
    prepared_dish = models.ForeignKey("PreparedDish", on_delete=models.SET(None), default=None, null=True)
    qty = models.IntegerField("Скiльки заготовлених використано", default=0)

    def __str__(self):
        return "Об'єкт %s" % self.ordered_dish.__str__

    class Meta:
        verbose_name = "Використана заготовлена страва"
        verbose_name_plural = "Використанi заготовленi страви"


class CafesOrderPart(models.Model):
    order = models.ForeignKey("Order", on_delete=models.CASCADE)
    cafe  = models.ForeignKey(Cafe, verbose_name="Кафе", on_delete=models.SET(None), null=True)
    should_start = models.DateTimeField("Приготування повинно початися", null=True)
    started = models.DateTimeField("Приготування почалося", null=True, blank=True, default=None)
    cooking_time = models.FloatField("Час приготування", null=True)
    waiting_time = models.FloatField("Час очiкування", null=True, default=0)
    is_done = models.BooleanField("Чи готово", default=False)

    def __str__(self):
        return "Об'єкт %s" % self.order.__str__

    class Meta:
        verbose_name = "Частина замовлення для кафе"
        verbose_name_plural = "Частини замовлень для кафе"
        unique_together = ("order", "cafe")


class DishInOrderForCafe(models.Model):
    cafes_order_part = models.ForeignKey("CafesOrderPart", on_delete=models.CASCADE, null=True)
    dish  = models.ForeignKey("Dish", verbose_name="Страва", on_delete=models.PROTECT, null=True)
    # prepared_used = models.FloatField("Використано заготовлених", default=0, null=True)
    qty   = models.IntegerField("Кiлькiсть", default=1)

    def __str__(self):
        return "Об'єкт %s" % self.cafes_order_part.__str__

    class Meta:
        verbose_name = "Cтрава в замовленнi для кафе"
        verbose_name_plural = "Список страв в кошиках"
        #ordering = ("order", "cafe", "dish",)
        #unique_together = ("order", "dish", "cafe")


class CouriersIngredientsTransitIngredient(models.Model):
    STATUSES = (
        ("in_process", "В процесі"),
        ("done", "Відправлено"),
    )
    ingr_import = models.ForeignKey("CouriersIngredientsTransit", on_delete=models.CASCADE, null=True)
    ingredient  = models.ForeignKey(IngredientName, on_delete=models.PROTECT, null=True) # + from_cafe
    from_cafe = models.ForeignKey(Cafe, on_delete=models.SET(None), null=True, default=None)
    amount = models.FloatField("К-ть", default=1)
    queue  = models.IntegerField("Место в очереди", default=0)
    status = models.CharField("Статус", choices=STATUSES, max_length=len(STATUSES[0][0]), null=True, blank=True)

    def __str__(self):
        try:
            return "%s з %s" % (self.ingredient.name, self.from_cafe.location.street_name)
        except: return ""

    class Meta:
        verbose_name = "частина транзиту iнгредiєнтiв"
        verbose_name_plural = "частини транзитiв iнгредiєнтiв"

class CouriersIngredientsTransit(models.Model):
    STATUSES = (
        ("not_chosen", "Кур'єр не обраний"),
        ("chosen", "Кур'єр обраний"),
        ("in_transit", "В дорозі"),
        ("done", "Доставлено"),
    )
    to_cafe = models.ForeignKey(Cafe, on_delete=models.PROTECT, null=True)
    courier = models.ForeignKey(User, on_delete=models.SET(None), null=True, default=None, blank=True)
    status = models.CharField("Статус", choices=STATUSES, default=STATUSES[0][0], max_length=20, blank=True)

    class Meta:
        verbose_name = "транзит iнгредiєнтiв"
        verbose_name_plural = "транзити iнгредiєнтiв"


class CouriersOrder(models.Model):
    STATUSES = (
        ("not_chosen", "Кур'єр не обраний"),
        ("chosen", "Кур'єр обраний"),
        ("in_transit", "В дорозі"),
        ("done", "Доставлено"),
    )
    order = models.ForeignKey("Order", on_delete=models.CASCADE)
    courier = models.ForeignKey(User, on_delete=models.SET(None), null=True, blank=True)
    status = models.CharField("Статус", choices=STATUSES, default=STATUSES[0][0], max_length=20)
    total_cost = models.FloatField("Вартiсть", default=0)

    def __str__(self):
        return "Замовлення для кур'ера вiд \"%s\"" % self.order.__str__

    class Meta:
        verbose_name = "замовлення для кур'єра"
        verbose_name_plural = "замовлення для кур'єра"


class CouriersOrderPart(models.Model):
    couriers_order = models.ForeignKey("CouriersOrder", on_delete=models.CASCADE, null=True, default=None)
    cafe  = models.ForeignKey(Cafe, on_delete=models.PROTECT, null=True)
    time = models.DateTimeField("Дата готовностi", null=True)
    query = models.IntegerField("Место в очереди", null=True)

    def __str__(self):
        return "Частина замовлення для кур'ера %s" % self.couriers_order.__str__

    class Meta:
        verbose_name = "частина замовлення для кур'єра"
        verbose_name_plural = "частини замовлень для кур'єрів"


class IngredientsInTransit(models.Model):
    ingr_import = models.ForeignKey("CouriersIngredientsTransit", on_delete=models.CASCADE, null=True)
    ingr_import_ingr = models.ForeignKey("CouriersIngredientsTransitIngredient", on_delete=models.CASCADE, null=True)
    ingredient = models.ForeignKey(Ingredient, verbose_name="Iнгредiєнт", on_delete=models.PROTECT, null=True)
    amount = models.FloatField("К-ть")

    class Meta:
        verbose_name = "iнгредiєнт в транзитi"
        verbose_name_plural = "iнгредiєнти в транзитi"