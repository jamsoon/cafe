# Generated by Django 2.0.5 on 2018-10-11 11:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dishes', '0011_auto_20181011_1424'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='couriersingredientstransitfromcafe',
            name='ingr_import',
        ),
        migrations.RemoveField(
            model_name='couriersingredientstransitfromcafe',
            name='ingredient',
        ),
        migrations.DeleteModel(
            name='CouriersIngredientsTransitFromCafe',
        ),
    ]
