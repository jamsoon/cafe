from django.db import models
from django.forms import ModelForm
from django.forms import inlineformset_factory
from django.contrib.auth.models import User
from django.utils.timezone import now as time_now
from dishes.models import *

class DishForm(ModelForm):
    class Meta:
        model = Dish
        fields = ["name", "image", "description", "price"]

class DishIngredientsForm(ModelForm):
    class Meta:
        model = DishIngredients
        exclude = ()



DishFormSet = inlineformset_factory(Dish, DishIngredients, form=DishIngredientsForm, extra=2)