from django.http import *
from django.db import *
import json
import sys
import urllib.request
import datetime
import random
from cafe.settings import *
import math
from django.urls import reverse
from django.conf import settings
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView, CreateView
from django.core.files.base import ContentFile
from django.contrib.auth.decorators import *
from django.contrib.auth import get_user_model
from django.utils.timezone import now as time_now
from django.utils.safestring import mark_safe
from django.db.models import Count, F
from django.middleware import csrf
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.decorators.csrf import ensure_csrf_cookie
from _auth.views import getUserCookieValue
import itertools
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import pytz
import copy
#from cafe.settings import MAPBOX
from django.utils import timezone
from django.db.models import Q

import operator
from cafe.views import get_ip

from django.contrib.auth.models import User
from authentication.serializers import UserSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from authentication.serializers import TokenCreateSerializer
from dishes.serializers import DishesSerializer
import djoser
from djoser.conf import settings
from djoser import utils
from rest_framework import generics, permissions, status, views, viewsets
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.exceptions import APIException
import django.core.exceptions as DjangoExceptions
from django.shortcuts import render
from django.contrib.auth.models import User

from dishes.models import Dish
from dishes.serializers import *
from authentication.permissions import *

from _auth.models import Profile

from dishes.models import *
from dishes.admin import *
from dishes.forms import *
from ingredients.models import *
from locations.models import *




def dishesList(request):
    dishesList = Dish.objects.all().order_by("-id")
    csrf_token = csrf.get_token(request)

    return render(request, "menu.html", locals())


def dishDescription(request):
    return render(request, "dish.html", {
        "request": request
    })


@authentication_classes([])
@permission_classes([])
class DishesList(APIView):
    permission_classes = ()

    def get(self, request, page=1):

        dishesPerPage = 30

        cats = []
        if "cats" in request.GET.keys():
            cats_ = []
            cats_.append(list(request.GET.get("cats").split(",")))

            for list_ in cats_:
                for i in list_:
                    try:
                        cats.append(int(i))
                    except Exception: continue

        print(cats)

        filters = {}

        if cats:
            filters.update({"category__category_id__in": cats})

        limit = 999999
        if "limit" in request.GET.keys():
            try:
                limit = int(request.GET.get("limit"))
            except Exception: pass

        dishes = Dish.objects.filter(is_active=True, **filters)
        pagination = Paginator(list(set(dishes))[:limit], dishesPerPage)

        dishes = pagination.get_page(page)

        serializer = DishesSerializer(dishes, many=True)

        return Response(serializer.data)


@authentication_classes([])
@permission_classes([])
class DishDetails(APIView):
    permission_classes = ()

    def get(self, request, id):
        dish = get_object_or_404(Dish, id=id)
        serializer = DishesSerializer(dish)
        return Response(serializer.data)



@authentication_classes([])
@permission_classes([])
class DishCategoriesList(APIView):
    def get(self, request):
        categories = CategoryName.objects.all()
        categoriesSerializer = CategoryNameSerializer(categories,  many=True).data
        return Response(categoriesSerializer)


class OrdersListForCourier(APIView):
    permission_classes = (IsCourier,)

    def get(self, request):
        try: ordered_data = request.GET.get("ordered_data")
        except Exception: ordered_data = 0.0
        try: ordered_by = request.GET.get("ordered_by")
        except Exception: ordered_by = "id"

        ordered_data = 0.0 if ordered_data == None else ordered_data

        ORDERED_BY = ("id", "-id", "time", "-time")
        if ordered_by not in ORDERED_BY:
            ordered_by = "time"# ORDERED_BY[0]

        if ordered_by in ("time", "-time"):
            try: time = datetime.datetime.fromtimestamp(float(ordered_data)) if ordered_by != "-time" and not ordered_data else datetime.datetime.fromtimestamp(32536799999+1000000000)
            except: time = datetime.datetime.fromtimestamp(0.0) if ordered_by == 'time' else datetime.datetime.fromtimestamp(32536799999+1000000000)
        else: time = datetime.datetime.fromtimestamp(0.0)

        filters = {}
        if ordered_by == "id":
            filters.update({"id__gt": ordered_data})
        elif ordered_by == "-id":
            filters.update({"id__lt": ordered_data})
        elif ordered_by == "time":
            filters.update({"time__gt": time})
        elif ordered_by == "-time":
            filters.update({"time__lt": time})

        #undoneOrders = CouriersOrder.objects.filter(courier=request.user).exclude(status=CouriersOrder.STATUSES[3][0])
        undoneOrders = []

        #if not undoneOrders:
        if True:
            ordersParts = CouriersOrderPart.objects.filter(**filters, couriers_order__status=CouriersOrder.STATUSES[0][0]).order_by(ordered_by)
            ordersParts_ = list(set(ordersParts.values_list("couriers_order", flat=True)))[:99] #[:6]
            ordersParts = CouriersOrderPart.objects.filter(couriers_order_id__in=ordersParts_).order_by(ordered_by)

            if ordersParts:
                ordersToSerialize = []
                for i in ordersParts:
                    # print(i)
                    id = i.couriers_order.pk
                    order = CouriersOrder.objects.get(id=id)
                    if order not in ordersToSerialize:
                        ordersToSerialize.append(order)

                orders = OrderForCourierSerializer([*ordersToSerialize], many=True).data

                return Response(orders)
            else:
                raise APIException("notFound")
            # except:
            #     raise APIException("error")
        else:
            raise APIException("hasUndoneOrders")


class OrderDetailsForCourier(APIView):
    permission_classes = (IsCourier,)

    def get(self, request, id):
        print(id)
        try:
            order = CouriersOrder.objects.get(id=id)

            if order.status == CouriersOrder.STATUSES[0][0]:
                serializer = OrderDetailsForCourierSerializer(order)
                return Response(serializer.data)
            else:
                raise APIException("alreadyChosen")

        except DjangoExceptions.ObjectDoesNotExist:
            raise APIException("notFound")


class IngredientsTransitListForCourier(APIView):
    permission_classes = (IsCourier,)

    def get(self, request):
        List = CouriersIngredientsTransit.objects.filter(status=CouriersIngredientsTransit.STATUSES[0][0])
        serializedList = CouriersIngredientsTransitSerializer(List, many=True).data

        return Response(serializedList)


class IngredientsTransitDetailsForCourier(APIView):
    permission_classes = (IsCourier,)

    def get(self, request, id):
        cit = CouriersIngredientsTransit.objects.get(id=id)
        if cit.status == CouriersIngredientsTransit.STATUSES[0][0]:
            serializedList = CouriersIngredientsTransitSerializer(cit).data
            return Response(serializedList)
        else:
            return APIException("alreadyChosen")



class CourierTakeOrder(APIView):
    permission_classes = (IsCourier, ) # IsCourierFree

    def post(self, request, id):
        user = request.user
        order = get_object_or_404(CouriersOrder, id=id)

        if order.courier_id or order.status == "done":
            raise APIException("alreadyChosen")
        else:
            order.status = "chosen"
            order.courier_id = user.pk

            try:
                order.save()
                return Response({"status": "ok"})
            except:
                order.status = "not_chosen"
                order.courier_id = None
                order.save()
                return Response({"status": "fail"})


class CourierCancelOrder(APIView):
    permission_classes = (IsCourier,)

    def post(self, request, id):
        user = request.user
        order = get_object_or_404(CouriersOrder, id=id)
        statuses = [st[0] for st in CouriersOrder.STATUSES]

        if order.courier_id != user.pk:
            raise APIException("chosenByOther")
        elif order.status in statuses[3]:
            raise APIException("alreadyDone")
        elif order.status in statuses[2]:
            raise APIException("orderInProcess")
        else:
            try:
                order.status = statuses[0]
                order.courier_id = None
                order.save()
                return Response({"status": "ok"})
            except:
                order.status = statuses[1]
                order.courier_id = user.pk
                order.save()
                return Response({"status": "fail"})



class CourierTakeIngrTransit(APIView):
    permission_classes = (IsCourier, ) # IsCourierFree

    def post(self, request, id):
        user = request.user
        order = get_object_or_404(CouriersIngredientsTransit, id=id)

        if order.courier_id or order.status == "done":
            raise APIException("alreadyChosen")
        else:
            order.status = "chosen"
            order.courier_id = user.pk

            try:
                order.save()
                return Response({"status": "ok"})
            except:
                order.status = "not_chosen"
                order.courier_id = None
                order.save()
                return Response({"status": "fail"})


class CourierCancelIngrTransit(APIView):
    permission_classes = (IsCourier,)

    def post(self, request, id):
        user = request.user
        order = get_object_or_404(CouriersIngredientsTransit, id=id)

        statuses = [st[0] for st in CouriersIngredientsTransit.STATUSES]

        if order.courier_id != user.pk:
            raise APIException("chosenByOther")
        elif order.status in statuses[3]:
            raise APIException("alreadyDone")
        elif order.status in statuses[2]:
            raise APIException("orderInProcess")
        else:
            try:
                order.status = statuses[0]
                order.courier_id = None
                order.save()
                return Response({"status": "ok"})
            except:
                order.status = statuses[1]
                order.courier_id = user.pk
                order.save()
                return Response({"status": "fail"})


class CourierUpdateLocation(APIView):
    permission_classes = (IsCourier,)

    def post(self, request):
        # try:
        # order = get_object_or_404(Order, id=id)
        user = self.request.user
        # is_courier = CouriersOrder.objects.filter(order_id=order.pk, courier_id=user.pk).values_list('id', flat=True)
        # if is_courier:
        data = json.loads(request.body.decode("utf-8"))
        lon = data['lon']
        lat = data['lat']

        try:
            lastPosition = LastPosition.objects.get_or_create(user_id=user.pk)[0]
            lastPosition.lat = lat
            lastPosition.lon = lon
            lastPosition.save()
        except Exception:
            pass

        orders = Order.objects.filter(couriersorder__courier_id=user.pk, couriersorder__status__in=["in_transit", "chosen"]).values_list('id', flat=True)
        print(orders)

        print(data)
        channel_layer = get_channel_layer()
        for orderID in orders:
            # print(orderID)
            # print("TRYING TO SEND TO GROUP user%d" % orderID)
            async_to_sync(channel_layer.group_send)(
                "user%d" % orderID,  # group
                {
                    "type": "sendLocation",  # sender
                    "courier": user.pk,
                    "lon": lon,
                    "lat": lat
                }
            )
        return Response()
        # else:
        #     raise APIException("notYourOrder")
        # except Exception:
        #     raise APIException("Sisya")


class CourierSubmitOrder(APIView):
    permission_classes = (IsCourier,)

    def post(self, request, id):
        user = request.user
        c_order = get_object_or_404(CouriersOrder, id=id, courier_id=user.pk)

        if c_order.status == "done":
            raise APIException("alreadyDone")
        else:
            c_order.status = "done"

            try:
                c_order.save()
                order = c_order.order
                c_orders = CouriersOrder.objects.filter(order_id=order.pk)
                statuses = []
                for i in c_orders:
                    statuses.append(i.status)
                statuses = list(set(statuses))
                if len(statuses) == 1 and statuses[0] == "done":
                    order.status = "done"
                    order.save()
                return Response({"status": "ok"})
            except:
                return Response({"status": "fail"})


class CourierMyOrders(APIView):
    permission_classes = (IsCourier,)


    def get(self, request):
        user = request.user
        orders = CouriersOrder.objects.filter(courier_id=user.pk, status__in=["chosen", "in_transit"])
        transits = CouriersIngredientsTransit.objects.filter(courier_id=user.pk, status__in=["chosen", "in_transit"])
        transitsList = CouriersIngredientsTransitSerializer(transits, many=True).data

        for i in transitsList:
            i.update({"type": "ingrTransit"})
        ordersList = OrderForCourierSerializer(orders, many=True).data
        for i in ordersList:
            i.update({"type": "order"})

        return Response(ordersList + transitsList)


class CourierTotalOrders(APIView):
    permission_classes = (IsCourier,)

    def get(self, request):
        user = request.user
        orders = CouriersOrder.objects.filter(status="not_chosen")
        qty = len(orders)

        return Response(json.dumps(qty))

class CourierTotalIngrTransit(APIView):
    permission_classes = (IsCourier,)

    def get(self, request):
        user = request.user
        orders = CouriersIngredientsTransit.objects.filter(status="not_chosen")
        qty = len(orders)

        return Response(json.dumps(qty))



def addDish(request):
    if request.method == "GET":
        return render(request, "add-dish.html", {
            "dish": DishForm,
            "inline": DishFormSet,
        })

    elif request.method == "POST":
        ingredient = DishForm(request.POST, request.FILES)
        print("request.POST", request.POST)
        formset = DishFormSet(request.POST)
        #print('formset', formset)
        is_error = False
        if not formset.is_valid():
            print('notv1')
        if not ingredient.is_valid():
            is_error = True
            print('notv2')

        print('1')
        if not is_error:
            f = formset.cleaned_data
            d = ingredient.cleaned_data

            createdDish = Dish.objects.create(
                name=d["name"],
                image=d["image"],
                description=d["description"],
                price=d["price"],
            )

            print('F', f)
            for ingredient in f:
                DishIngredients.objects.create(
                    dish=createdDish,
                    ingredient=ingredient["ingredient"],
                    amount=ingredient["amount"],
                )

            return HttpResponseRedirect(request.META.get('HTTP_REFERER') or "/")
        else:
            return render(request, "add-dish.html", {
                "dish": ingredient,
                "inline": formset,
            })


def cart(request):
    user = User.objects.get(id=request.user.id ) if request.user.id else None
    userCookieValue = getUserCookieValue(request, user)

    dishesList = DishInCart.objects.filter(
        user = user,
        user_cookie = userCookieValue,
    ).order_by("-id")

    csrf_token = csrf.get_token(request)
    response = render(request, "cart.html", locals())
    response.set_cookie("uh", userCookieValue, expires=datetime.datetime.now(pytz.UTC) + datetime.timedelta(weeks=+4))


    timeNow = datetime.datetime.now(pytz.UTC) + datetime.timedelta(seconds=40)

    allIngredients_ = Ingredient.objects.all()

    allIngredients = []

    for i in allIngredients_:
        date = i.date
        if i.date > timeNow - datetime.timedelta(days=i.name.shelfLife):
            allIngredients.append(i)

    print("ALL INGREIDENT_ LENGTH:", len(allIngredients_))
    print("ALL INGREIDENT LENGTH:", len(allIngredients))




    return response

@login_required
def acceptOrder(request):
    user = request.user
    location = Profile.objects.get(user_id=user.pk).location
    allOrderedDishes = DishInCart.objects.filter(user_id=user.pk)

    if not allOrderedDishes:
        return HttpResponseRedirect(reverse('cart'))

    return render(request, "accept.html", {
        "lat": location.lat if location else None,
        "lng": location.lon if location else None,
        "street_name": location.street_name if location and location.street_name else "",
        "home_details": location.home_details if location and location.home_details else "",
    })

def orderDetails(request, id):
    user = request.user
    order = get_object_or_404(Order, id=id)
    if order.user_id != user.pk:
        return HttpResponseForbidden
    dishes = DishInOrder.objects.filter(order_id=order.pk)
    cafes = set([i.cafe.location for i in CafesOrderPart.objects.filter(order_id=order.pk)])
    print("cafes", cafes)

    cafes = LocationSerializer(cafes, many=True).data
    print("cafes1", cafes)

    return render(request, "order-details.html", {
        "order": order,
        "dishes": dishes,
        "cafes": cafes
    })


class CreateOrder(APIView):
    permission_classes = (permissions.IsAuthenticated,) # permissions.IsAuthenticated,

    def get(self, request):
        user = request.user
        userProfile = Profile.objects.get(user_id=user.pk)

        USER_LOCATION = {}
        print('GET DATA:', request.GET)

        for i in ["street_name", "home_details", "lon", "lat"]:
            if i not in request.GET.keys():
                raise APIException()

        try:
            USER_LOCATION.update({
                "street_name": request.GET.get("street_name"),
                "home_details": request.GET.get("home_details"),
                "lon": float(request.GET.get("lon")),
                "lat": float(request.GET.get("lat"))
            })

        except Exception:
            raise APIException()

        print("USER LOCATION:", USER_LOCATION)

        allCafes = Cafe.objects.all()
        allCafesList = [cafe for cafe in allCafes]
        allCafesReleaseTime = {}
        allOrderedDishes = DishInCart.objects.filter(user_id=user.pk)
        allOrderedDishesList = [dish for dish in allOrderedDishes]
        allOrderedDishesDict = {dish: dish for dish in allOrderedDishesList}
        #allDishesList = [dish.dish for dish in allOrderedDishes]
        allDishesList = [dish for dish in Dish.objects.filter(is_active=True)]
        allDishIngredients= {} # {dish: {ingr1: amount, ingr2: amount}}
        allIngredients_ = Ingredient.objects.all()
        allIngredients = []
        allIngredientsByCafe = {} # {cafe: {ingr1: amount, ingr2: amount}}
        allPreparedDishes = PreparedDish.objects.filter(dish_id__in=allDishesList)
        allPreparedDishes_ = {}
        allPreparedDishesByCafe = {cafe: {} for cafe in allCafesList}
        allImportingIngredients = CouriersIngredientsTransitIngredient.objects.filter(
            ingr_import__status__in = [s[0] for s in CouriersIngredientsTransit.STATUSES[:-1]]
        )

        timeNow = datetime.datetime.now(pytz.UTC) + datetime.timedelta(seconds=40)

        # удаление просроченных ингредиентов
        for i in allIngredients_:
            if i.date > timeNow - datetime.timedelta(days=i.name.shelfLife):
                allIngredients.append(i)

        print("allIngredients:", allIngredients)
        print("length:", len(allIngredients))

        print("allOrderedDishes", allOrderedDishes)
        if not allOrderedDishes:
            return Response({
                "status": "Кошик порожній!"
            })

        def changeIngredientsOrdering(orig, mode="byIngr"):
            result = {}
            if mode == "byIngr":
                for cafe, v in orig.items():
                    for ingr, amount in v.items():
                        if ingr not in result.keys(): result.update({ingr: {}})
                        result[ingr].update({cafe: amount})

            return result

        def combinationsRemoveDuplicates(orig, x):
            allSets = set()
            for i in orig:  # для каждого (('d1', 'c1'), ('d2', 'c1'), ('d3', 'c1'))
                _set = set()
                for n in i:  # для каждого ('d1', 'c1')
                    _set.add(n[0])  # добавляем название блюда

                if len(_set) < len(x):
                    # если длина множества меньше чем к-во блюд => какое-то блюдо повторяется и "i" - неправильный
                    continue
                else:
                    allSets.add(i)
            return allSets


        for cafe in allCafesList:
            last = CafesOrderPart.objects.filter(cafe_id=cafe.pk).last()
            if last:
                allCafesReleaseTime.update({cafe: last.started + datetime.timedelta(minutes=last.cooking_time)})
            else:
                allCafesReleaseTime.update({cafe: timeNow})


        for dish in allDishesList:
            di = DishIngredients.objects.filter(dish_id=dish.pk)
            if dish not in allDishIngredients.keys():
                allDishIngredients.update({
                    dish: {}
                })
            for i in di:
                allDishIngredients[dish].update({
                    i.ingredient: i.amount
                })

        for ingredient in allIngredients:
            cafe = ingredient.cafe
            ingr_ = ingredient.name
            amount = ingredient.totalAmount

            # ingrsInUse = IngredientsInUse.objects.filter(ingredient__cafe_id=cafe.pk, ingredient_id=ingredient.pk)
            # for iiu in ingrsInUse:
            #     amount -= iiu.amount

            # ключами есть кафе
            if cafe not in allIngredientsByCafe.keys():
                allIngredientsByCafe.update({
                    ingredient.cafe: {}
                })
            if ingr_ not in allIngredientsByCafe[cafe].keys():
                allIngredientsByCafe[cafe].update({
                    ingr_: amount
                })
            else:
                allIngredientsByCafe[cafe][ingr_] += amount

        for cafe in allCafesList:
            if cafe not in allIngredientsByCafe.keys():
                allIngredientsByCafe.update({cafe: {}})


        for ingrTrans in allImportingIngredients:
            cafe = ingrTrans.from_cafe
            ingr = ingrTrans.ingredient.name
            amount = ingrTrans.amount

            if cafe not in allIngredientsByCafe.keys():
                allIngredientsByCafe.update({cafe: {}})

            if ingr not in allIngredientsByCafe[cafe].keys():
                allIngredientsByCafe[cafe].update({
                    ingr: 0
                })

            allIngredientsByCafe[cafe][ingr] -= amount

            print(amount, "x", ingr, "in", cafe)

        for k, v in allDishIngredients.items():
            print(k, ":", v)
        for orderPart in CafesOrderPart.objects.filter(is_done=False):
            for diofc in DishInOrderForCafe.objects.filter(cafes_order_part_id=orderPart.pk):
                used = PreparedDishesUsed.objects.filter(ordered_dish_id=diofc.pk)
                try:
                    for ingrName, amount in allDishIngredients[diofc.dish].items(): # fixme allDishIngredients
                        cafe = diofc.cafes_order_part.cafe
                        try:
                            if used:
                                allIngredientsByCafe[cafe][ingrName] -= amount * (diofc.qty - used[0].qty)
                            else:
                                allIngredientsByCafe[cafe][ingrName] -= amount * diofc.qty
                        except: pass
                except: pass

        # чтобы никакие ингредиенты не ушли в минус, и алгоритм отработал верно
        for cafe, ingrs in allIngredientsByCafe.items():
            for ingr, amount in ingrs.items():
                if amount < 0:
                    allIngredientsByCafe[cafe][ingr] += 0 - allIngredientsByCafe[cafe][ingr]

        # for cafe, ingrs in allIngredientsByCafe.items():
        #     print(cafe)
        #     for i, amount in ingrs.items():
        #         print(i, type(i), ":", amount)

        # allIngredientsByIngr = changeIngredientsOrdering(allIngredientsByCafe, "byIngr")

        for cafe, ingrs in allIngredientsByCafe.items():
            print(cafe)
            for i, amount in ingrs.items():
                print(" "*2, i, ":", amount)

        for prDish in allPreparedDishes:
            cafe = prDish.cafe
            dish = prDish.dish

            if dish not in allPreparedDishesByCafe[cafe].keys():
                allPreparedDishesByCafe[cafe].update({dish: 0})

            if prDish.add_time + datetime.timedelta(minutes=dish.prepared_lifetime) > allCafesReleaseTime[cafe]: # BILO timeNow
                allUsed = PreparedDishesUsed.objects.filter(prepared_dish_id=prDish.pk)
                used = 0
                for prDishUsed in allUsed:
                    used += prDishUsed.qty
                allPreparedDishesByCafe[cafe][dish] += prDish.qty - used

                allPreparedDishes_.update({prDish: prDish.qty - used})

        print("PREPAREDDISHES")
        for cafe, dishes in allPreparedDishesByCafe.items():
            print(cafe)
            for dish, qty in dishes.items():
                print(" "*2, dish, ":", qty)

        # время езды от одного кафе к другому (как (от X к Y), так и (от Y к X))
        # {(cafe1, cafe2): time}
        cafesTransDurations = {}
        # время езды от каждого кафе к клиенту
        # {cafe1: time}
        cafesToClientDurations = {}

        cafesDistances = MapboxDirectionsMatrix.matrix([{
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [c.location.lon, c.location.lat]
            }} for c in allCafes
        ], profile="driving").json()

        for durations in cafesDistances["durations"]:
            # получаем кафе, от которого мы меряем
            me = None
            for i, d in enumerate(durations):
                if d == 0: me = allCafesList[i]

            for i, d in enumerate(durations):
                if d != 0:
                    # добавляем в словарь время перемещения от me до this
                    this = allCafesList[i]
                    cafesTransDurations.update({(me, this): d})

        url = "https://api.mapbox.com/directions-matrix/v1/mapbox/driving/" \
              "%(coords)s?sources=%(sources)s&destinations=%(destination)s&access_token=%(token)s" % {
            "coords": ";".join([
                "%s,%s" % (str(i.location.lon), str(i.location.lat)) for i in allCafesList] +
                ["%s,%s" % (str(USER_LOCATION["lon"]), str(USER_LOCATION["lat"]))]),
            "sources": ";".join(["%d" % i for i in range(0, len(allCafesList))]),
            "destination": "%d" % len(allCafesList),
            "token": MAPBOX_KEY,
        }
        cafesToClientDistances = json.loads(urllib.request.urlopen(url).read().decode("utf-8"))

        print("URL:", url)

        for i, d in enumerate(cafesToClientDistances["durations"]):
            this = allCafesList[i]
            cafesToClientDurations.update({this: d[0]})

        print("cafesToClientDurations", cafesToClientDurations)
        print("cafesTransDurations", cafesTransDurations)

        def timeBetweenCafes(items):
            combs = []
            stop = False
            i = 0
            totalTime = 0
            while not stop:
                try:
                    combs.append((items[i], items[i + 1]))
                    i += 1
                except Exception:
                    stop = True

            for i in combs:
                totalTime += cafesTransDurations[i]

            return totalTime + (30 * len(combs)) # добавляем полминуты на обработку



        r = list(itertools.product(allOrderedDishesList, allCafesList))
        print("r:", r)
        print("len(r):", len(r))
        print("allOrderedDishesList", allOrderedDishesList)
        print("len(allOrderedDishesList):", len(allOrderedDishesList))
        res = list(itertools.combinations(r, len(allOrderedDishesList)))
        print("res", res)
        allSets = combinationsRemoveDuplicates(res, allOrderedDishesList)

        # print("allSets:", len(allSets))
        # for i, r in enumerate(allSets):
        #     if i >= 3: break  # ставим лимит на 3
        #     print("    ", r)

        selectedCafesCombination = []

        print("allSets", allSets)
        print(len(allSets))

        for combination in allSets:
            print("COMBINATION", combination)

            #######################################################
            # ПОИСК НАИЛУЧШЕЙ КОМБИНАЦИИ ДЛЯ ИМПОРТА ИНГРЕДИЕНТОВ #
            #######################################################

            #print("====" * 20)
            ingredientsByCafe = copy.deepcopy(allIngredientsByCafe)
            # ingredientsByIngr = {}
            for dish, cafe in combination:
                if dish.dish in allDishIngredients.keys():
                    for ingr, amount in allDishIngredients[dish.dish].items():
                        if  not ingr in ingredientsByCafe[cafe].keys():
                            ingredientsByCafe[cafe].update({ingr: 0.0})

                        qty = 0
                        if dish.dish in allPreparedDishesByCafe.keys():
                            needed = dish.qty
                            preparedQtyTotal = allPreparedDishesByCafe[dish.dish]
                            if preparedQtyTotal >= needed:
                                qty += 0
                            else:
                                qty += needed - preparedQtyTotal

                        else:
                            qty = dish.qty

                        ingredientsByCafe[cafe][ingr] -= amount * qty # amount * allOrderedDishesDict[dish].qty

            # ingredientsByIngr = changeIngredientsOrdering(ingredientsByCafe, "byIngr")

            missingIngredientsByCafe = [] # (cafe, ingr, amount)
            for cafe, v in ingredientsByCafe.items():
                for ingr, amount in v.items():
                    if amount < 0:
                        missingIngredientsByCafe.append((cafe, ingr, -amount))
            #print("comb:", combination)
            selectedIngrTransComb = [] # {(((toCafe, ingr, amount), fromCafe), ...): [[([fromCafe, toCafe], time), ([fromCafe2, toCafe2], time2)], points]}
            if missingIngredientsByCafe:
                #print("missing", missingIngredientsByCafe)

                r = list(itertools.product(missingIngredientsByCafe, allCafesList))
                r = list(itertools.combinations(r, len(missingIngredientsByCafe)))
                ingredientsTransCombs = combinationsRemoveDuplicates(r, missingIngredientsByCafe)
                rightTransCombs = []
                print(len(ingredientsTransCombs))
                # проверка каждого элемента комбинации
                for comb in ingredientsTransCombs:
                    ingredients = copy.deepcopy(allIngredientsByCafe) # именно allIngredientsByCafe
                    #ingredients.pop(comb[0][0][0])
                    right = True
                    for (toCafe, ingr, amount), fromCafe in comb:
                        # если отправка из кафе-получателя или такого ингредиента нету (т.е. 0)
                        if toCafe == fromCafe or ingr not in ingredients[fromCafe].keys():
                            right = False
                            break
                        # если всё ок - отнимаем со склада отправителя нужное к-во ингредиентов
                        else: ingredients[fromCafe][ingr] -= amount

                    for cafe, v in ingredients.items():
                        # пробегаемся по складу - если ни один ингредиент не ушел в минус,
                        # комбинация верная
                        if not all(i >= 0 for i in v.values()):
                            right = False

                    if right:
                        rightTransCombs.append(comb)
                    else: continue

                transCombsDurations = {}
                for comb in rightTransCombs:
                    #print("rightComb", comb)
                    comb_ = {}# []
                    to_from = {} # {to: (from, from)}
                    usedCafes = set()
                    combForCafe = {}
                    for (toCafe, ingr, amount), fromCafe in comb:
                        # time = cafesTransDurations[(fromCafe, toCafe)]
                        usedCafes.add(fromCafe)
                        if fromCafe not in combForCafe.keys():
                            combForCafe.update({fromCafe: {}})
                        combForCafe[fromCafe].update({ingr: amount})

                        if toCafe not in to_from.keys():
                            to_from.update({toCafe: set([])})

                        to_from[toCafe].add(fromCafe)


                    selected = {} #

                    for id_, (to, from_) in enumerate(to_from.items()):
                        x = (tuple(from_), to, id_)
                        p = list(itertools.permutations(from_, len(from_)))
                        for i in p:
                            i = list(i)
                            i.append(to)
                            time = timeBetweenCafes(i)
                            if x not in selected.keys():
                                selected.update({x: (None, -1)})
                            if selected[x][1] < 0:
                                selected[x] = (i, time)
                            else:
                                if selected[x][1] > time:
                                    selected[x] = (i, time)

                    points = sum([time for comb, time in selected.values()]) / len(selected.values()) * math.sqrt(len(selected.values()))

                    transCombsDurations.update({
                        comb: [[v for k, v in selected.items()], points]
                    })
                    # for k, v in selected.items():
                    #     print("    ", k)
                    #     print("    ", v)

                # print("transCombsDurations:")
                # for k, v in transCombsDurations.items():
                #     print("    ", k)
                #     items = v[0]
                #     points = v[1]
                #     for i in items:
                #         print("    " * 2, i)
                #     print("    " * 2, points)

                minPoints = -1
                for k, v in transCombsDurations.items():
                    points = v[1]
                    # if len(k) > 3:
                    #     print("\n\n")
                    #     for i in k:
                    #         print(" "*2, i)
                    #     print()
                    #     for n, i in enumerate(v):
                    #         if n == 0:
                    #             for ii in i:
                    #                 print(" "*2, ii)
                    # print("K:", k)
                    # print("V:", v)
                    # print("POINTS:", points)
                    if minPoints < 0 or points < minPoints:
                        #selectedIngrTransComb = {k: v}
                        selectedIngrTransComb.append((k, v))
                        minPoints = points
                        continue

                # print("selectedIngrTransComb")
                # for k, v in selectedIngrTransComb.items():
                #     print("    ", k)
                #     print("    ", v)

            #############################################################
            # С ИНГРЕДИЕНТАМИ РАЗОБРАЛИСЬ.
            #############################################################

            if not missingIngredientsByCafe or selectedIngrTransComb:
                print("1")
                if selectedIngrTransComb:
                    selectedIngrTransComb = selectedIngrTransComb[-1]

                cafe_dishes = {}
                data_for_cafe = {}
                for dish, cafe in combination:
                    if cafe not in cafe_dishes.keys():
                        cafe_dishes.update({cafe: []})
                    cafe_dishes[cafe].append(dish)

                allTime = {} # {cafe: (totalCookingTime, totalWaitingTime, readyDateTime)}

                for cafe, dishesForCafe in cafe_dishes.items():
                    totalCookingTime = 0 # время, когда кафе будет занято
                    totalWaitingTime = 0 # время, когда будет готово (не важно, было кафе занято или нет)

                    for orderedDish in dishesForCafe:
                        dish = orderedDish.dish
                        qty = orderedDish.qty
                        prepared = 0

                        preparedDishes = allPreparedDishesByCafe[cafe]
                        if dish in preparedDishes.keys() and preparedDishes[dish] > 0:
                            prepared_ = preparedDishes[dish]
                            if prepared_ >= qty:
                                prepared = qty
                            else:
                                prepared = prepared_

                        totalWaitingTime += prepared * dish.prepared_cooking_time  + \
                                            dish.cooking_time * (qty - prepared)
                        totalCookingTime += dish.cooking_time * (qty - prepared)

                    releaseTime = timeNow
                    if timeNow < allCafesReleaseTime[cafe]:
                        releaseTime = allCafesReleaseTime[cafe]
                    readyDateTime = releaseTime + datetime.timedelta(minutes=totalWaitingTime)

                    allTime.update({cafe: (totalCookingTime, totalWaitingTime, readyDateTime)})

                cafe_time = []
                # [1,2,3,4] => [(1,2), (3,4)], [(1,2,3), (4,)], [(1,), (2, 3, 4)], [(1, 3), (2, 4)] + наоборот
                # думаете, вы что-то здесь поймёте? проще переписать всё заново
                def cafesToClientCombinations(x):
                    xs = list(itertools.permutations(x, len(x)))

                    y = set()
                    z = set()

                    for xi in xs:
                        for i in range(0, len(xs)):
                            f = tuple(itertools.islice(xi, i + 1))
                            y.add(f)

                    for i in y:
                        #print("I", i)
                        k = []
                        if len(i) < len(x):
                            for j in y:
                                error = False
                                for l in i:
                                    if l in j:
                                        error = True
                                        break

                                if not error:
                                    k.append(j)

                            for ki in k:
                                if len(ki) + len(i) < len(x):
                                    def s(g):
                                        if len(g) > len(x) - len(ki):
                                            return True

                                        for v in ki:
                                            if v in list(g):
                                                return True

                                    p = list(itertools.filterfalse(s, k))

                                    for ii in range(1, len(x) - len(ki) + 1):

                                        rx = tuple(itertools.combinations(p, ii))
                                        rx_final = []
                                        allVals = []
                                        for rxi in rx:
                                            for rxii in rxi:
                                                for rxiii in rxii:
                                                    allVals.append(rxii)

                                        allValsSet = set(allVals)

                                        for rxi in rx:
                                            for rxii in rxi:
                                                rx_final.append(rxii)

                                        if len(allValsSet) == len(allVals):
                                            srtd = tuple(sorted((i, ki, *tuple(rx_final))))
                                            srtd_len = set([])
                                            for srtdi in srtd:
                                                for srtdii in srtdi:
                                                    srtd_len.add(srtdii)
                                            if len(srtd_len) == len(x):
                                                # print(" "*2, srtd)
                                                z.add(srtd)
                                else:
                                    z.add(tuple(sorted((i, ki))))
                        else:
                            z.add(tuple(sorted((i,))))

                    return z

                def calculatePointsCafesToClientComb(comb):
                    combs = []
                    stop = False
                    i = 0
                    lastTime = 0
                    while not stop:
                        try:
                            combs.append((comb[i], comb[i + 1]))
                            i += 1
                        except Exception:
                            stop = True

                    for i in combs:
                        c1 = i[0][1]
                        c2 = i[1][1]
                        d1 = lastTime or datetime.datetime.timestamp(i[0][2]) # время освобождения кафе
                        d2 = datetime.datetime.timestamp(i[1][2])
                        dur = cafesTransDurations[(c1, c2)]
                        willBeInCafe2 = d1 + dur
                        if d2 < willBeInCafe2:
                            lastTime = willBeInCafe2
                        else:
                            lastTime = d2

                    if not lastTime:
                        lastTime = datetime.datetime.timestamp(comb[-1][2]) + cafesToClientDurations[comb[-1][1]]
                    else:
                        lastTime += cafesToClientDurations[comb[-1][1]]

                    return lastTime


                for cafe, data in allTime.items():
                    cafe_time.append((cafe.id, cafe, data[2])) # , cafe, data[2]

                cafesToClientCombinations_ = cafesToClientCombinations(cafe_time)
                cafesToClientCombinationsWTime = []
                selectedCafesToClientCombination = []
                for i in cafesToClientCombinations_:
                    #print()
                    #print("ASF", i)
                    time = []
                    for ii in i:
                        points_ = calculatePointsCafesToClientComb(ii)
                        time.append((
                            datetime.datetime.fromtimestamp(points_, tz=pytz.UTC) - timeNow
                        ).total_seconds())
                    points = ( sum(time) / len(time) ) * math.sqrt(2 * len(time))
                    #print(" ", points)

                    if not selectedCafesToClientCombination:
                        selectedCafesToClientCombination.append((i, points))
                    else:
                        i = selectedCafesToClientCombination[-1][0]
                        min_points = selectedCafesToClientCombination[-1][1]
                        if points < min_points:
                            #print("BETTER")
                            selectedCafesToClientCombination.append((i, points))

                    #selectedCafesToClientCombination = [selectedCafesToClientCombination[-1]]

                    #print(selectedCafesToClientCombination)

                selectedCafesToClientCombination = selectedCafesToClientCombination[-1]
                if selectedCafesToClientCombination:
                    i = selectedCafesToClientCombination[0]
                    points = selectedCafesToClientCombination[1]
                    x = (combination, allTime, selectedIngrTransComb, i)
                    # print(x)
                    # print(selectedIngrTransComb)
                    # print("ASFSFAS", ((k, v) for k, v in selectedIngrTransComb.items()))
                    # for k, v in selectedIngrTransComb.items():
                    #     print(" ", k)
                    #     for vi in v:
                    #         print(" "*3, vi)
                    if not selectedCafesCombination:
                        selectedCafesCombination.append((x, points))
                    else:
                        i = selectedCafesCombination[-1][0]
                        min_points = selectedCafesCombination[-1][1]
                        if points < min_points:
                            selectedCafesCombination.append((x, points))
                    #break

                    #print("POINTS:", points/60)
                #print("cafe_time", cafe_time)
                #print("SELECTED")
            #break

        if selectedCafesCombination:
            print("SELECTED")
            print(selectedCafesCombination[-1], "\n")

            combs = selectedCafesCombination[-1][0]
            cafesCombination = combs[0]
            cafesTime = combs[1] # {cafe: (totalCookingTime, totalWaitingTime, readyDateTime)}
            ingrTransCombination = combs[2]
            cafesToClientCombination = combs[3]
            print("РАСПРЕДЕЛЕНИЕ МЕЖДУ КАФЕ:")

            for i in cafesCombination:
                print(" "*2, i)
            print("ВРЕМЯ ГОТОВНОСТИ КАФЕ:")
            for cafe, v in cafesTime.items():
                print(" "*2, cafe)
                for i in v:
                    print(" "*4, i)

            print("ТРАНСПОРТИРОВКА ИНГРЕДИЕНТОВ:")
            try:
                k = ingrTransCombination[0] # ((to_cafe, ingr, amount), from_cafe)
                v = ingrTransCombination[1] # ((from_cafe, to_cafe))
            except:
                k, v = None, None
            print(" "*2, k)
            print(" "*4, v)

            print("ОТ КАФЕ К КЛИЕНТУ:")
            print(cafesToClientCombination)
            for i in cafesToClientCombination:
                print(" "*2, "*"*5)
                for ii in i:
                    print(" "*2, ii)

            channel_layer = get_channel_layer()

            location = None
            try:
                location = Location.objects.get_or_create(
                    street_name = USER_LOCATION["street_name"],
                    home_details = USER_LOCATION["home_details"],
                    lat = USER_LOCATION["lat"],
                    lon = USER_LOCATION["lon"],
                )[0]
            except DjangoExceptions.MultipleObjectsReturned:
                location = Location.objects.filter(
                    street_name = USER_LOCATION["street_name"],
                    home_details = USER_LOCATION["home_details"],
                    lat = USER_LOCATION["lat"],
                    lon = USER_LOCATION["lon"],
                )[0]


            order = Order.objects.create(
                user_id = user.pk,
                location_id = location.pk,
                datetime = datetime.datetime.now(tz=pytz.UTC),
                total_cost = sum([dish.dish.price * dish.qty for dish in allOrderedDishes])
            )


            for cafe, val in cafesTime.items():
                cookingTime = val[0]
                waitingTime = val[1]
                readyDateTime = val[2]

                cafe_op = CafesOrderPart.objects.create(
                    order_id = order.pk,
                    cafe_id = cafe.pk,
                    should_start = readyDateTime - datetime.timedelta(minutes=cookingTime),
                    started = readyDateTime - datetime.timedelta(minutes=cookingTime),
                    cooking_time = cookingTime,
                    waiting_time = waitingTime
                )

                for dish, in_cafe in cafesCombination:
                    if in_cafe == cafe:
                        print(allOrderedDishesDict[dish])
                        print(allOrderedDishesDict[dish].qty)
                        print(dish.dish)
                        print(type(dish.dish))
                        dio = DishInOrder.objects.create(
                            order_id = order.pk,
                            dish_id = dish.dish.pk,
                            qty = dish.qty,
                        )
                        diofc = DishInOrderForCafe.objects.create(
                            cafes_order_part = cafe_op,
                            dish = dish.dish,
                            qty = dish.qty,
                        )
                        dishesLeft = allOrderedDishesDict[dish].qty

                        for prDish in allPreparedDishes.filter(cafe_id=in_cafe.pk, dish_id=dish.pk):
                            if prDish in allPreparedDishes_.keys():
                                prDishQtyTotal = allPreparedDishes_[prDish]
                                if prDishQtyTotal > 0:
                                    prDishQtyToUse = 0

                                    if dishesLeft >= prDishQtyTotal:
                                        prDishQtyToUse = prDishQtyTotal
                                    elif dishesLeft < prDishQtyTotal:
                                        prDishQtyToUse = dishesLeft

                                    PreparedDishesUsed.objects.create(
                                        ordered_dish_id = diofc.pk,
                                        prepared_dish_id = prDish.pk,
                                        qty = prDishQtyToUse,
                                    )

                async_to_sync(channel_layer.group_send)(
                    'cafe' + str(cafe.id),  # group
                    {
                        "type": "sendNewOrderCafe",  # sender
                        "text": json.dumps({
                            "message": "[МЕНЕДЖЕР] У вашому кафе нове замовлення!",
                            "data": json.dumps(CafeNewOrderSerializer(cafe_op).data)
                        })
                    }
                )



            for i in cafesToClientCombination:

                cour_o = CouriersOrder.objects.create(
                    order_id = order.pk,
                    total_cost = 0.0,
                )

                for id, (_, cafe, time) in enumerate(i):

                    # сколько нужно забрать денег у получателя
                    for dish, inCafe in cafesCombination:
                        if inCafe == cafe:
                            cour_o.total_cost += dish.dish.price * dish.qty

                    cour_op = CouriersOrderPart.objects.create(
                        couriers_order_id = cour_o.pk,
                        cafe = cafe,
                        time = time,
                        query = id,
                    )

                cour_o.save()

                async_to_sync(channel_layer.group_send)(
                    'couriers',  # group
                    {
                        "type": "sendNewOrderCourier",  # sender
                        "order": OrderForCourierSerializer(cour_o).data
                    }
                )


            if v and k:
                for (cafesPaths, _) in v[0]:
                    destination = cafesPaths[-1]
                    from_cafes = cafesPaths[:-1]

                    print()
                    print(destination)
                    print(" " * 2, from_cafes)

                    cit = CouriersIngredientsTransit.objects.create(
                        to_cafe = destination,
                    )

                    for (to_cafe, ingr, amount), from_cafe in k:
                        if to_cafe == destination and from_cafe in from_cafes:
                            id = [i for i, v in enumerate(from_cafes) if v == from_cafe][0]
                            CouriersIngredientsTransitIngredient.objects.create(
                                ingr_import_id = cit.pk,
                                ingredient_id = ingr.pk,
                                from_cafe_id = from_cafe.pk,
                                amount = amount,
                                queue=int(id)
                            )

            # обновляем местоположение в профиле
            userLocation = userProfile.location or Location()
            userLocation.lon, userLocation.lat = USER_LOCATION["lon"], USER_LOCATION["lat"]
            userLocation.street_name = USER_LOCATION["street_name"]
            userLocation.home_details = USER_LOCATION["home_details"]
            userLocation.save()
            userProfile.location_id = userLocation.pk
            userProfile.save()

            # чистим корзину
            for i in allOrderedDishes:
                i.delete()

            return Response({
                            "status": "ok",
                            "order": order.pk
                           })



        return Response({
            "status": "Помилка! Брак інгредієнтів на складах!"
        })





# кустарно и тупо. на момент написания этой части в вопросах апи был зеленее зелёного
@ensure_csrf_cookie
def api(request):
    user = User.objects.get(id=request.user.id ) if request.user.id else None
    userCookieValue = getUserCookieValue(request, user)

    if request.method == "POST":
        args = request.POST
        action = args["action"] if args["action"] else None
        status = ""
        json_data = None

        # addToCart
        if action == "addToCart":
            if args["id"] and args["id"].isdigit():
                filters = {}
                if user and user.is_authenticated:
                    filters.update({"user": user})
                else:
                    filters.update({"user_cookie": userCookieValue})

                existingDishes = DishInCart.objects.filter(
                    **filters
                )

                for dish in existingDishes:
                    if int(dish.dish.id) == int(args["id"]):
                        status = "Товар вже знаходиться в кошику"
                        break

                if not status:
                    DishInCart.objects.create(
                        dish = Dish.objects.get(id=args["id"]),
                        qty  = 1,
                        user = user,
                        user_cookie = userCookieValue,
                    )
                    status = "ok"

        # changeQty
        if action == "changeQty":
            newVal = args["newVal"] if args["newVal"] else None
            if newVal and newVal.isdigit() and int(newVal) >= 1:
                newVal = int(newVal)
                if args["id"] and args["id"].isdigit():
                    dish = DishInCart.objects.get(id=int(args["id"]))
                    if dish:
                        dish.qty = newVal
                        dish.save()
                        status = "ok"


        if action == "remove":
            id = args["id"]
            dishInCart = get_object_or_404(DishInCart, id=id)

            isRightUser = False

            if user and user.is_authenticated and dishInCart.user_id == user.pk:
                isRightUser = True

            elif not user or not user.is_authenticated:
                if dishInCart.user_cookie == userCookieValue:
                    isRightUser = True


            if isRightUser\
                    or (request.COOKIES["uh"] and dishInCart.user_cookie == request.COOKIES["uh"]):
                dishInCart.delete()
                status = "ok"
            status = "fail"


        response = HttpResponse(json.dumps({'status': status, "data": json_data}), content_type="application/json")
        response.set_cookie("uh", userCookieValue, expires=datetime.datetime.now(pytz.UTC) + datetime.timedelta(weeks=+4))
        return response


def test(request, id):
    return render(request, 'test.html', {
        "id": id
    })