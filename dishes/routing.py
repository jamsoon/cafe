from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^order/alert/$', consumers.newOrder),
    url(r"^order/courier-location/$", consumers.CourierLocation)
]