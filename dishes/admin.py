from django.contrib import admin
from dishes.models import *

class DishIngredientsInline(admin.TabularInline):
    extra = 2
    model = DishIngredients

class CategoryInline(admin.TabularInline):
    extra = 1
    model = Category

class DishAdm(admin.ModelAdmin):
    ordering = ("name", "price")
    list_display = ("name", )
    inlines = (DishIngredientsInline, CategoryInline)

class DishInCartAdm(admin.ModelAdmin):
    ordering = ("dish", "user", "user_cookie")
    list_display = ("dish", "user", "user_cookie")

class DishInOrderInline(admin.TabularInline):
    extra = 0
    model = DishInOrder

class DishInOrderForCafeInline(admin.TabularInline):
    extra = 0
    model = DishInOrderForCafe

class CafesOrderPartAdm(admin.ModelAdmin):
    inlines = (DishInOrderForCafeInline,)

class CafesOrderPartInline(admin.TabularInline):
    extra = 0
    model = CafesOrderPart

class OrderAdm(admin.ModelAdmin):
    list_display = ("user", "datetime", "total_cost")
    inlines = (CafesOrderPartInline,)

#class CouriersIngredientsTransitIngredientAdm(admin.ModelAdmin):


class CouriersIngredientsTransitIngredientInline(admin.TabularInline):
    extra = 0
    model = CouriersIngredientsTransitIngredient

class CouriersIngredientsTransitAdm(admin.ModelAdmin):
    inlines = (CouriersIngredientsTransitIngredientInline,)


class CouriersOrderPartInline(admin.TabularInline):
    extra = 0
    model = CouriersOrderPart

class CouriersOrderAdm(admin.ModelAdmin):
    inlines = (CouriersOrderPartInline,)


admin.site.register(Dish, DishAdm)
admin.site.register(PreparedDish)
admin.site.register(DishInCart, DishInCartAdm)
admin.site.register(Order, OrderAdm)
admin.site.register(CouriersOrderPart)
admin.site.register(CategoryName)
admin.site.register(Category)
admin.site.register(CouriersIngredientsTransit, CouriersIngredientsTransitAdm)
admin.site.register(CouriersIngredientsTransitIngredient)
admin.site.register(CouriersOrder, CouriersOrderAdm)
admin.site.register(CafesOrderPart, CafesOrderPartAdm)
admin.site.register(IngredientsInTransit)
admin.site.register(DishInOrder)