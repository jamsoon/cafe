from django.http import *
from django.db import *
import json
from cafe.settings import *
from django.conf import settings
from django.shortcuts import render, render_to_response, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView, CreateView
from django.core.files.base import ContentFile
from django.contrib.auth.decorators import *
from django.contrib.auth import get_user_model
from django.utils.timezone import now as time_now

from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

from cafes.models import *
from cafes.admin import *

from dishes.models import *
from dishes.admin import *

from ingredients.models import *
from ingredients.admin import *
from ingredients.forms import *


def get_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def home(request):
    return render(request, "home.html") # base.html