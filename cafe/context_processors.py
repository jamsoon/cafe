from django.contrib.auth.models import User
from _auth.models import *


def profile(request):
    try:
        if request.user and request.user.is_authenticated:
            profile = Profile.objects.get(user_id=request.user.pk)
            return {
                "profile": profile
            }
        else:
            return {}
    except Exception:
        return {}