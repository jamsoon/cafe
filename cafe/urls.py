"""cafe URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin as admin_
from admin import *

from admin import urls as adminUrls
from manager import urls as managerUrls

from django.conf.urls import *
from cafe import views
from django.conf import settings
from django.urls import path, re_path, include
from django.contrib.auth.views import logout
from django.contrib.auth import views as django_auth_views
from django.contrib.auth import urls as authUrls

import rest_framework_jwt
#from rest_framework_jwt import views as rest_framework_jwt_views

from ingredients import views as ingredientsViews
from dishes import views as dishesViews
from _auth import views as authViews
from cafes import views as cafesViews
from authentication import views as authenticationViews
from statistics_ import views as statisticsViews

urlpatterns = [
    path('admin/', admin_.site.urls),
    #path('admin/', include(adminUrls.urlpatterns)),
    path("manager/", include(managerUrls.urlpatterns)),
    url(r"^add-ingredient/$", ingredientsViews.ingredient, name="add-ingredient"),
    url(r"^add-ingredientName/$", ingredientsViews.ingredientName, name="add-ingredientName"),
    url(r"^add-dish/$", dishesViews.addDish, name="add-dish"),
    url(r"^menu/$", dishesViews.dishesList, name="menu"),
    url(r"^dish/$", dishesViews.dishDescription, name="dishDescription"),
    url(r"^cart/$", dishesViews.cart, name="cart"),
    url(r"^accept-order/$", dishesViews.acceptOrder, name="acceptOrder"),
    path("order-details/<int:id>/", dishesViews.orderDetails, name="orderDetails"),
    url(r"^$", views.home, name="home"),

    # path("dish/", dishesViews.Dishes.as_view(), name="dish"),
    path("api/courier/orders_list/", dishesViews.OrdersListForCourier.as_view()),
    path("api/courier/order_details/<int:id>/", dishesViews.OrderDetailsForCourier.as_view()),
    path("api/courier/take_order/<int:id>/", dishesViews.CourierTakeOrder.as_view()),
    path("api/courier/cancel_order/<int:id>/", dishesViews.CourierCancelOrder.as_view()),
    path("api/courier/update_location/", dishesViews.CourierUpdateLocation.as_view()),
    path("api/courier/order/submit/<int:id>/", dishesViews.CourierSubmitOrder.as_view()),
    path("api/courier/order/my/", dishesViews.CourierMyOrders.as_view()),
    path("api/courier/order/total/", dishesViews.CourierTotalOrders.as_view()),

    path("api/courier/ingredients_transit_list/", dishesViews.IngredientsTransitListForCourier.as_view()),
    path("api/courier/ingredients_transit_details/<int:id>/", dishesViews.IngredientsTransitDetailsForCourier.as_view()),
    path("api/courier/take_ingredients_transit/<int:id>/", dishesViews.CourierTakeIngrTransit.as_view()),
    path("api/courier/cancel_ingredients_transit/<int:id>/", dishesViews.CourierCancelIngrTransit.as_view()),
    path("api/courier/ingredients_transit/total/", dishesViews.CourierTotalIngrTransit.as_view()),

    path("api/menu/dishes_list/<int:page>/", dishesViews.DishesList.as_view()),
    path("api/menu/categories_list/", dishesViews.DishCategoriesList.as_view()),
    path("api/menu/dish_description/<int:id>/", dishesViews.DishDetails.as_view()),

    path("api/order/create/", dishesViews.CreateOrder.as_view(), name="createOrder"),

    path("api/user/data/", authViews.UserData.as_view()),

    path("api/statistics/get/", statisticsViews.GetStatistics.as_view(), name="managerStatisticsGet"),


    url(r"^api/dish/$", dishesViews.api, name="apiDish"),
    url(r"^api/ingredient/$", ingredientsViews.api, name="apiIngredient"),

    url(r"^api/auth/", include("djoser.urls")),
    url(r"^api/auth/", include("djoser.urls.authtoken")),
    url(r"^api/auth/", include("djoser.urls.jwt")),
    #url(r"^api/auth/token/login1/", authenticationViews.token_request),

    url(r"^user/signup/$", authViews.signup, name="signup"),
    url(r"^user/login/$", django_auth_views.login, {'template_name': 'login.html'}, name="login"),
    url(r"^user/logout/$", logout, {"next_page": "/"}, name="logout"),
    url(r"^user/profile/$", authViews.profile, name="profile"),

    path('test/<int:id>/', dishesViews.test, name='room'),
    #url(r"^api/token/login/$", authenticationViews.CustomTokenCreateView.as_view()),


    # url(r"^api/login/$", rest_framework_jwt.views.obtain_jwt_token, name="apiLogin"),
]

from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()