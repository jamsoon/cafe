from django.contrib import admin
from ingredients.models import *
from django.db import models


# class IngredientNameAdm(admin.ModelAdmin):
#     ordering = ("name",)
#     list_display = ("name", "shelfLife")
#
# class IngredientAdm(admin.ModelAdmin):
#     ordering = ("name",)
#     list_display = ("name",)
class IngredientNameAdm(admin.ModelAdmin):
    ordering = ("name",)
    list_display = ("name", "shelfLife")

class IngredientAdm(admin.ModelAdmin):
    date_hierarchy = "date"
    ordering = ("name", "packagesAmount",)
    list_display = ("name", "cafe", "packagesAmount", "goodsAmount", "price", "date")
    exclude = ("totalAmount", "is_overdued", )


admin.site.register(Units)
admin.site.register(IngredientName, IngredientNameAdm)
admin.site.register(Ingredient, IngredientAdm)