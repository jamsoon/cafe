from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.utils.timezone import now as time_now
from ingredients.models import *

class IngredientNameForm(ModelForm):
    class Meta:
        model = IngredientName
        fields = ["name", "shelfLife", "buyingUnits", "sellingUnits"]

class IngredientForm(ModelForm):
    class Meta:
        model = Ingredient
        fields = ["name", "date", "cafe", "packagesAmount", "goodsAmount", "price"]