from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User
from django.utils.timezone import now as time_now


class Units(models.Model):
    name = models.CharField("Одиниця виміру", unique=True, max_length=30)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Одиниця виміру"
        verbose_name_plural = "Одиниці виміру"
        ordering = ("name",)




class IngredientName(models.Model):
    name  = models.CharField("Назва", unique=True, max_length=200)
    shelfLife = models.FloatField("Термін придатності (дiб)", default=0)
    # Покупается 10 пачек майонеза по 200 гр. каждая. В базу данных записывается
    buyingUnits = models.ForeignKey("Units", verbose_name="Одиниці виміру купуємого товару", null=True, on_delete=models.SET_NULL, related_name="units")
    sellingUnits = models.ForeignKey("Units", verbose_name="Одиниці виміру продаваного товару", null=True, on_delete=models.SET_NULL)
    #selUns_in_buyUns = models.FloatField("Продаваного товару в купуємому", default=1.0, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Iнгредієнт"
        verbose_name_plural = "Список назв iнгредієнтiв"
        ordering = ("name",)


class Ingredient(models.Model):
    name = models.ForeignKey("IngredientName", verbose_name="Назва", on_delete=models.CASCADE)
    date  = models.DateTimeField("Дата виробництва", default=time_now,)
    cafe = models.ForeignKey("cafes.Cafe", verbose_name="Кафе", on_delete=models.SET_NULL, null=True)
    packagesAmount = models.IntegerField("Кількість \"упаковок\"", default=1)
    goodsAmount    = models.FloatField("К-ть товарiв в \"упаковцi\"", default=1.0) # "В одной коробке находится 10 конфет" = в одном package находится 10 goods
    totalAmount    = models.FloatField("Всього товару", default=0)
    is_overdued = models.BooleanField("Прострочено", default=False, blank=True)
    price = models.FloatField("Цiна", default=0) # Цена за один package

    # def save(self, *args, **kwargs):
    #     try: old = Ingredient.objects.get(id=self.id)
    #     except Exception: old = None
    #
    #     self.totalAmount = self.goodsAmount * self.packagesAmount
    #     #print(self.goodsAmount, "*", self.packagesAmount, "=", self.totalAmount)
    #     # if old:
    #     #
    #     #     oldUsed = (old.packagesAmount * old.goodsAmount) - old.totalAmount
    #     #
    #     #     self.totalAmount = self.totalAmount - oldUsed
    #
    #     super(Ingredient, self).save(*args, **kwargs)


    def __str__(self):
        return self.name.name

    class Meta:
        verbose_name = "Інгредієнт"
        verbose_name_plural = "Список iнгредієнтiв"
        ordering = ("name", "packagesAmount",)

