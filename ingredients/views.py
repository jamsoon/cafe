from django.http import *
from django.db import *
import json
from cafe.settings import *
from django.conf import settings
from django.shortcuts import render, render_to_response, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView, CreateView
from django.core.files.base import ContentFile
from django.contrib.auth.decorators import *
from django.contrib.auth import get_user_model
from django.utils.timezone import now as time_now
from django.views.decorators.csrf import ensure_csrf_cookie

from ingredients.models import *
from ingredients.forms import *
from ingredients.admin import *

def ingredient(request):
    if request.method == "GET":
        return render(request, "add-ingredient.html", {
            "ingredient": IngredientForm,
        })
    elif request.method == "POST":
        ingredient = IngredientForm(request.POST)
        if ingredient.is_valid():
            d = ingredient.cleaned_data
            Ingredient.objects.create(
                name=d["name"],
                date=d["date"],
                cafe=d["cafe"],
                packagesAmount=d["packagesAmount"],
                goodsAmount=d["goodsAmount"],
                price=d["price"],
            )

            return HttpResponseRedirect(request.META.get('HTTP_REFERER') or "/")
        else:
            return render(request, "add-ingredient.html", {"ingredient": ingredient})


def ingredientName(request):
    if request.method == "GET":
        return render(request, "add-ingredientName.html", {
            "ingredientName": IngredientNameForm,
        })
    elif request.method == "POST":
        print("request.POST:\n", request.POST)
        ingredientName = IngredientNameForm(request.POST)

        is_child_window = False
        if "is_child" in request.POST and request.POST["is_child"][0] == "1":
            is_child_window = True

        if ingredientName.is_valid():
            d = ingredientName.cleaned_data

            created = IngredientName.objects.create(
                name=d["name"],
                shelfLife=d["shelfLife"],
                buyingUnits=d["buyingUnits"],
                sellingUnits=d["sellingUnits"],
            )

            if is_child_window:
                return HttpResponse('''
                <script type=\"application/javascript\">
                    var option = window.opener.document.createElement("option");
                    option.value = '%s';
                    option.innerHTML = '%s';
                    select = window.opener.document.getElementById("id_name");
                    select.appendChild(option);

                    // убираем аттрибут selected у элементов списка
                    var elements = window.opener.document.getElementById("id_name").options;
                    for(var i = 0; i < elements.length; i++){
                      elements[i].selected = false;
                    }

                    option.selected = true;
                    window.close()
                </script>''' % (created.id, created.name))
            else:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER') or "/")
        else:
            return render(request, "add-ingredientName.html", {"ingredientName": ingredientName})


@ensure_csrf_cookie
def api(request):
    if request.method == "GET":
        render_to_response("api_page.html")

    elif request.method == "POST":
        args = request.POST
        print(args)
        action = args["action"] if args["action"] else None
        status = ""
        data   = list()

        if action == "getList":
            allIngredients = IngredientName.objects.all()
            for i in allIngredients:
                data.append({"id": i.id, "name": i.name})
            status = "ok"

        return HttpResponse(json.dumps({'status': status, 'data': data}), content_type="application/json")
    return 0