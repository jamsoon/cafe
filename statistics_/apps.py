from django.apps import AppConfig


class StatisticsConfig(AppConfig):
    name = 'statistics_'
    verbose_name = "статистика"
    verbose_name_plural = "статистика"
