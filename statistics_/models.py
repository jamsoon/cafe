from django.db import models
from django.utils.timezone import now as time_now

class Statistics(models.Model):
    date = models.DateField("дата", default=time_now, unique=True)
    ordered_dishes_qty = models.IntegerField("к-ть замовлених страв", default=0)
    bought_ingredients = models.FloatField("вартiсть замовлених інгредієнтів", default=0)
    used_ingredients = models.FloatField("вартiсть витрачених інгредієнтів", default=0)
    overdued_ingredients = models.FloatField("вартiсть зіпсованих інгредієнтів", default=0)
    total_profit = models.FloatField("сума прибутку", default=0)

    def __str__(self):
        return str(self.date)

    class Meta:
        verbose_name = "статистика"
        verbose_name_plural = "статистика"