from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.http import HttpResponseForbidden

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import APIException
from rest_framework import generics, permissions, status, views, viewsets

from _auth.models import Profile
from .models import *
from .serializers import *


class GetStatistics(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        user = request.user
        userProfile = Profile.objects.get(user_id=user.pk)
        userRole = userProfile.role

        if not userRole == "manager":
            return HttpResponseForbidden()

        stat = Statistics.objects.all()

        return Response(StatisticsSerializer(stat, many=True).data)