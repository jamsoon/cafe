from django.contrib.auth.models import User
from rest_framework import serializers

from .models import *

class StatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Statistics
        exclude = []