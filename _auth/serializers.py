from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Profile

from locations.serializers import LocationSerializer
from locations.models import Location

from dishes.models import *

class ProfileSerializer(serializers.ModelSerializer):
    location = serializers.SerializerMethodField()
    works_in_cafe = serializers.SerializerMethodField()

    def get_location(self, profile):
        try:
            return LocationSerializer(Location.objects.get(id=profile.location_id)).data
        except Exception: return None

    def get_works_in_cafe(self, profile):
        try:
            return LocationSerializer(Location.objects.get(id=profile.worksInCafe.location_id)).data
        except Exception: return None

    class Meta:
        model = Profile
        fields = ("phone_number", "role", "location", "works_in_cafe")

class UserSerializer(serializers.ModelSerializer):
    profile = serializers.SerializerMethodField()
    other = serializers.SerializerMethodField()

    userProfile = None

    def get_profile(self, user):
        try:
            self.userProfile = Profile.objects.get(user_id=user.id)
            return ProfileSerializer(self.userProfile).data
        except Exception: return None

    def get_other(self, user):
        if self.userProfile and self.userProfile.role == "courier":
            orders = CouriersOrder.objects.filter(status="not_chosen").count()
            ingrTrans = CouriersIngredientsTransit.objects.filter(status="not_chosen").count()
            return {
                "total_orders": orders + ingrTrans,
            }
        else:
            return {}

    class Meta:
        model = User
        fields = ("id", "first_name", "last_name", "email", "profile", "other")