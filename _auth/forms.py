from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordResetForm
from django.contrib.auth.models import User

from .models import *

class SignUpForm(UserCreationForm):
    email = forms.EmailField(label="E-Mail", required=True, help_text="*")
    phone_number = forms.CharField(label="Номер телефона", max_length=13, required=False, help_text="")

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields.pop('password2')
        self.fields.pop('email')

        # for fieldname in ["username", "password1"]:
        #     self.fields[fieldname].help_text = "*"

        self.fields["first_name"].required = True
        self.fields["phone_number"].required = True

    class Meta:
        model = User
        fields = ("username", "phone_number", "password1", "first_name")


class ChangeForm(forms.ModelForm):
    # first_name = forms.CharField(label="Ім'я", max_length=20)
    # last_name = forms.CharField(label="Прізвище", max_length=40)
    # email = forms.EmailField(label="E-Mail", required=True, help_text="*")
    phone_number = forms.CharField(label="Номер телефона", max_length=13, required=False, help_text="",
                                   widget=forms.TextInput(attrs={'placeholder': 'Номер телефона'}))

    def __init__(self, *args, **kwargs):
        user = kwargs["instance"]
        profile = Profile.objects.get(user_id=user.pk)

        kwargs.update(initial={
            'phone_number': profile.phone_number
        })

        super(ChangeForm, self).__init__(*args, **kwargs)



    class Meta:
        model = User
        fields = ["first_name", "last_name", "phone_number"]