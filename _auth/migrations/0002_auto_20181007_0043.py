# Generated by Django 2.0.5 on 2018-10-06 21:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('_auth', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='role',
            field=models.CharField(blank=True, choices=[('courier', 'Курьер'), ('manager', 'Менеджер')], default=None, max_length=7, null=True, verbose_name='Роль'),
        ),
        migrations.DeleteModel(
            name='Roles',
        ),
    ]
