from django.http import *
from django.db import *
import random
import string
import json
from cafe.settings import *
from django.conf import settings
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView, CreateView
from django.core.files.base import ContentFile
from django.contrib.auth.decorators import *
from django.contrib.auth import get_user_model
from django.utils.timezone import now as time_now

from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.forms import UserCreationForm
from rest_framework.exceptions import APIException
from rest_framework.views import APIView
from rest_framework.response import Response

from django.db.models import Q

from django.contrib.auth.models import User
from rest_framework import generics
from _auth.serializers import UserSerializer

from cafe.views import get_ip

from .models import Profile
from django.contrib.auth import login, authenticate
from _auth.forms import *
from dishes.models import *


class UserData(APIView):

    def get(self, request):
        user = UserSerializer(request.user).data
        return Response(user)


class UserListAPIView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


def getUserCookieValue(request, user=None):
    if not user or not user.is_authenticated:
        print("request.COOKIES", request.COOKIES)
        cookie_value = request.COOKIES.get("uh") if "uh" in request.COOKIES and request.COOKIES.get("uh") not in [None, "None"] else ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))
        print(cookie_value)

        return cookie_value
    else: return None


def setDishesUser(request, user):
    '''
    Если пользователь авторизируется - все его товары в корзине становятся "собственностью" user, а не значения куков
    '''
    from dishes.models import DishInCart
    dishesList = DishInCart.objects.filter(user_cookie = getUserCookieValue(request))
    for dish in dishesList:
        dish.user = user
        dish.user_cookie = None
        dish.save()


def signup(request):
    _user = request.user
    if not _user.is_authenticated:
        if request.method == 'POST':
            form = SignUpForm(request.POST)
            if form.is_valid():
                if not _user.is_authenticated:
                    user = form.save()
                    cd = form.cleaned_data
                    user.refresh_from_db()  # load the profile instance created by the signal
                    user.profile.phone_number = cd.get("phone_number")
                    user.email = "" # cd.get("email")
                    user.save()
                    setDishesUser(request, user)
                    password = form.cleaned_data.get("password1")
                    user = authenticate(username=user.username, password=password)
                    login(request, user)
                    nextPage = "/"
                    if "next" in request.GET.keys():
                        nextPage = request.GET.get("next")
                    response = HttpResponseRedirect(nextPage)
                    response.delete_cookie("uh")
                    return response
                return HttpResponse("You're already logged in!")
        else:
            form = SignUpForm()
    else:
        return HttpResponse("You're already logged in!")

    return render(request, 'signup.html', {'form': form})


def _login(request):
    return HttpResponse("")

@login_required
def profile(request):
    user = request.user
    profile = get_object_or_404(Profile, user_id=user.pk)
    orders = Order.objects.filter(user_id=user.pk).order_by("-id")

    form = ChangeForm(instance=user)

    if request.method == "GET":
        print(form)

    elif request.method == "POST":
        form = ChangeForm(request.POST, instance=user)
        if form.is_valid():
            data = form.cleaned_data

            user.first_name = data["first_name"]
            user.last_name = data["last_name"]
            # user.email = data["email"]
            profile.phone_number = data["phone_number"]
            user.save()
            profile.save()



    return render(request, "profile.html", {
        "profile": profile,
        "orders": orders,
        "form": form,
    })