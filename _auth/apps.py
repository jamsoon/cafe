from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = '_auth'
    verbose_name = 'Доповнення до аутентифікації і авторизації'
