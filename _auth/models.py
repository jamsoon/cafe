from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from cafes.models import Cafe
from locations.models import Location

# class Roles(models.Model):
#     name = models.CharField("Назва", max_length=50)
#     _name = models.CharField("Назва (лат)", max_length=30)
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         verbose_name = "Роль"
#         verbose_name_plural = "Ролi"

class Profile(models.Model):
    ROLES = (
        ("courier", "Курьер"),
        ("manager", "Менеджер")
    )

    user = models.OneToOneField(User, verbose_name="Користувач", on_delete=models.CASCADE)
    phone_number = models.CharField("Номер телефона", max_length=13, blank=True, null=True)
    location = models.ForeignKey(Location, blank=True, null=True, on_delete=models.SET(None))
    worksInCafe = models.ForeignKey(Cafe, blank=True, null=True, on_delete=models.SET(None))
    role = models.CharField("Роль", choices=ROLES, null=True, blank=True, default=None, max_length=7)


    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "Профiль користувача"
        verbose_name_plural = "Профiлi користувачiв"

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()