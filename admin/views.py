from django.http import *
from django.db import *
import json
import sys
import urllib.request
import datetime
import random
from cafe.settings import *
import math
from django.conf import settings
from django.shortcuts import render, render_to_response, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView, CreateView
from django.core.files.base import ContentFile
from django.contrib.auth.decorators import *
from django.contrib.auth import get_user_model
from django.utils.timezone import now as time_now
from django.utils.safestring import mark_safe
from django.db.models import Count
from django.db.models import F
from django.middleware import csrf
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.decorators.csrf import ensure_csrf_cookie
from _auth.views import getUserCookieValue
import itertools
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import pytz
import copy
#from cafe.settings import MAPBOX
from django.utils import timezone
from django.db.models import Q

import operator
from cafe.views import get_ip

from django.contrib.auth.models import User
from authentication.serializers import UserSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from authentication.serializers import TokenCreateSerializer
from dishes.serializers import DishesSerializer
import djoser
from djoser.conf import settings
from djoser import utils
from rest_framework import generics, permissions, status, views, viewsets
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.exceptions import APIException
import django.core.exceptions as DjandoExceptions




def home(request):
    return HttpResponse()