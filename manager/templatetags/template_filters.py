import re
from django import template
import datetime as dt
from django.utils.timezone import now as time_now

register = template.Library()

@register.filter
def sub(value, arg):
    try:
        return value - arg
    except Exception: return 0

@register.filter
def increaseTimeByMins(time, mins):
    return time + dt.timedelta(minutes=mins)

@register.filter
def increaseTimeWithDays(time, days):
    return time + dt.timedelta(days=days)

@register.filter
def ifNone(value, nullVal):
    if not value:
        return nullVal
    else:
        return value

@register.filter
def toStr(text):
    try:
        return str(text)
    except Exception:
        return text

@register.filter
def getByKey(iterable, key):
    try:
        return iterable[key]
    except Exception: return ""

@register.filter
def getObjField(obj, name):
    try:
        return getattr(obj, name)
    except Exception: return ""

@property
def is_past_due(time):
    return dt.dt.now() < time

@register.filter
def minsToTime(mins):
    days = int(mins / 60 / 24)
    hours = int(mins / 60) - days * 24
    minutes = mins - (hours * 60) - (days * 24 * 60)

    result = []

    if int(days) >= 1:
        result.append("%d д" % days)

    if int(hours) >= 1:
        result.append("%d год" % hours)

    if int(minutes) >= 1:
        result.append("%d хв" % minutes)

    if len(result) > 1:
        return ", ".join(result)
    else:
        return result[0]

@register.filter
def floatWithDot(fl):
    try:
        return str(fl).replace(",", ".")
    except: return ""

@register.simple_tag()
def isAlmostOverdued(manufacturedDate, shelfLife):
    warningThreshold = dt.timedelta(days=(shelfLife / 100 * 5))
    minThreshold = dt.timedelta(hours=3)

    if warningThreshold < minThreshold:
        warningThreshold = minThreshold
    
    timeNow = time_now()
    try:
        lifetimeLeft = (manufacturedDate + dt.timedelta(days=shelfLife)) - timeNow
    except OverflowError:
        return False

    if lifetimeLeft <= warningThreshold:
        return True
    else:
        return False


@register.simple_tag
def nextPages_tag(value, nextPages, i, *args, **kwargs):
    totalPages = value
    nextPages = len(nextPages)

    ret = totalPages - nextPages + i + 1

    return ret