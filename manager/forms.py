from django.db import models
from django import forms
from django.forms import *
from django.forms import inlineformset_factory
from django.contrib.auth.models import User
from django.utils.timezone import now as time_now
import pytz
from dishes.models import *
import datetime

class PreparedDishForm(ModelForm):
    class Meta:
        model = PreparedDish
        fields = ("dish", "cafe", "qty")

class IngredientNameForm(ModelForm):
    class Meta:
        model = IngredientName
        fields = ["name", "shelfLife", "buyingUnits", "sellingUnits"]

class IngredientForm(ModelForm):
    date = DateTimeField(widget=DateTimeInput, input_formats=['%Y-%m-%dT%H:%M'])
    class Meta:
        model = Ingredient
        fields = ["name", "date", "cafe", "packagesAmount", "goodsAmount", "totalAmount", "price"]

class IngredientTotalAmountForm(ModelForm):
    class Meta:
        model = Ingredient
        fields = ["totalAmount"]


class DishForm(ModelForm):
    class Meta:
        model = Dish
        fields = ["name", "image", "description", "calorie", "weight", "cooking_time",
                  "prepared_cooking_time", "prepared_lifetime", "price"]


class DishIngredientsForm(ModelForm):
    class Meta:
        model = DishIngredients
        exclude = ()


class IngrTransitForm(Form):
    from_cafe = ModelChoiceField(queryset=Cafe.objects.all())
    to_cafe = ModelChoiceField(queryset=Cafe.objects.all())

class IngrTransitIngrForm(ModelForm):
    ingredient_ = ModelChoiceField(queryset=IngredientName.objects.all(), label="Інгредієнт")
    amount_ = FloatField(min_value=0, label="К-ть")

    class Meta:
        model = CouriersIngredientsTransitIngredient
        fields = ["ingredient_", "amount_"]


class IngrWithTimeChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
         return "%s (%s)" % (obj.name.name, obj.date.astimezone(tz=pytz.timezone("Europe/Kiev")).strftime("%d.%m %H:%M:%S"))

class IngrTransitIngrSubmitForm(ModelForm):
    ingredient_ = IngrWithTimeChoiceField(queryset=Ingredient.objects.all(), label="Iнгредiєнт")
    amount_ = FloatField(min_value=0, label="К-ть")
    class Meta:
        model = IngredientsInTransit
        fields = ["ingredient_", "amount_"]

# class IngrTransitIngrSubmitFormSet(BaseInlineFormSet):
#     def __init__(self, cafe, *args, **kwargs):
#         super(IngrTransitIngrSubmitFormSet, self).__init__(*args, **kwargs)
#
#         for form in self.forms:
#             form.fields['ingredient'].queryset = Ingredient.objects.filter(is_overdued=False, cafe_id=cafe)

class IngrTransitIngrSubmitFormSet(BaseInlineFormSet):
    def __init__(self, cafe, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # self.queryset = Ingredient.objects.filter(is_overdued=False, cafe_id=cafe)
        #print("FORMS", self.forms)

        for form in self.forms:
            allIngredients_ = Ingredient.objects.filter(cafe_id=cafe)
            ingredientsIDs = []
            timeNow = datetime.datetime.now(pytz.UTC)
            # удаление просроченных ингредиентов
            for i in allIngredients_:
                if i.date > timeNow - datetime.timedelta(days=i.name.shelfLife):
                    ingredientsIDs.append(i.pk)

            allIngredients = Ingredient.objects.filter(id__in=ingredientsIDs)

            form.fields['ingredient_'].queryset = allIngredients


DishFormSet = inlineformset_factory(Dish, DishIngredients, form=DishIngredientsForm, extra=2)
IngrTransitIngrFormSet_ = inlineformset_factory(CouriersIngredientsTransit, CouriersIngredientsTransitIngredient,
                                               form=IngrTransitIngrForm, extra=2) # formset=IngrTransitIngrFormSet,

IngrTransitIngrSubmitFormSet = inlineformset_factory(CouriersIngredientsTransit, model=IngredientsInTransit,
                                                     form=IngrTransitIngrSubmitForm,
                                                     formset=IngrTransitIngrSubmitFormSet, extra=2) #formset=IngrTransitIngrSubmitFormSet,