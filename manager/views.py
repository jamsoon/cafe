from django.http import *
from django.db import *
import json
import sys
import urllib.request
import datetime
import random
from cafe.settings import *
import math
from django.conf import settings
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import transaction
from django.urls import reverse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.views.generic.edit import FormView, CreateView
from django.core.files.base import ContentFile
from django.contrib.auth.decorators import *
from django.contrib.auth import get_user_model
from django.utils.timezone import now as time_now
from django.utils.safestring import mark_safe
from django.db.models import Count
from django.db.models import F
from django.middleware import csrf
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.decorators.csrf import ensure_csrf_cookie
from _auth.views import getUserCookieValue
import itertools
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import pytz
import copy
import re

from rest_framework.views import APIView
from rest_framework.response import Response

#from cafe.settings import MAPBOX
from django.utils import timezone
from django.db.models import Q

import operator
from cafe.views import get_ip

from django.contrib.auth.models import User
from authentication.serializers import UserSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from authentication.serializers import TokenCreateSerializer
from dishes.serializers import DishesSerializer
import djoser
from djoser.conf import settings
from djoser import utils
from rest_framework import generics, permissions, status, views, viewsets
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.exceptions import APIException
import django.core.exceptions as DjandoExceptions
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from _auth.models import Profile
from dishes.models import *
from ingredients.models import *

from .forms import *




def home(request):
    return ordersList(request, 1)

@login_required
def ordersList(request, page):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe

    ordersPerPage = 20

    orders = CafesOrderPart.objects.filter(cafe_id=cafe.pk).order_by("is_done", "-id")
    pagination = Paginator(orders, ordersPerPage)

    orders = pagination.get_page(page)

    return render(request, "manager/orders-list.html", {"orders": orders, "cafeAddress": cafe.location.street_name})


@login_required
def orderDetails(request, id):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe

    order = get_object_or_404(CafesOrderPart, pk=id)

    if request.method == "GET":
        dishes = DishInOrderForCafe.objects.filter(cafes_order_part_id=order.pk)

        return render(request, "manager/order-details.html", {"order": order,
                                                              "dishes": dishes,
                                                              "cafeAddress": cafe.location.street_name})

    elif request.method == "POST":
        act = request.POST["action"]
        status = False

        if act == "toggleStatus":
            if order.is_done == False:
                order.is_done = True
                order.save()
                status = True
            else:
                status = False

        return HttpResponse(json.dumps({"status": status}))


@login_required
def preparedDishesList(request, page):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    dishesPerPage = 30

    dishes = PreparedDish.objects.filter(cafe_id=cafe.pk).order_by("-id")
    pagination = Paginator(dishes, dishesPerPage)

    dishes = pagination.get_page(page)


    return render(request, "manager/preparedDishes-list.html", {"dishes": dishes,
                                                                "timeNow": timeNow,
                                                                "cafeAddress": cafe.location.street_name})



@login_required
def preparedDishDetails(request, id):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    dish = get_object_or_404(PreparedDish, pk=id)

    form = PreparedDishForm()

    return render(request, "manager/preparedDish-details.html", {"dish": dish,
                                                                 "form": form,
                                                                 "timeNow": timeNow,
                                                                 "cafeAddress": cafe.location.street_name})

@login_required
def preparedDishAdd(request):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    if request.method == "GET":
        form = PreparedDishForm()

    if request.method == "POST":
        form = PreparedDishForm(request.POST)
        # print(1)
        # print(form)
        form.cafe = cafe
        if form.is_valid():
            d = form.cleaned_data
            print(d)
            print(d["dish"])
            PreparedDish.objects.create(
                cafe = cafe,
                dish = d["dish"],
                qty = d["qty"]
            )
            return HttpResponseRedirect(reverse('managerPreparedDishesList', args=(1,)))


    return render(request, "manager/preparedDish-add.html", {"form": form,
                                                             "timeNow": timeNow,
                                                             "cafeAddress": cafe.location.street_name,
                                                             "cafeID": cafe.id})


@login_required
def ingredientsList(request, page):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    ingredientsPerPage = 20

    orders = Ingredient.objects.filter(cafe_id=cafe.pk).order_by("-id")
    pagination = Paginator(orders, ingredientsPerPage)

    orders = pagination.get_page(page)

    return render(request, "manager/ingredients-list.html", {"ingredients": orders,
                                                             "timeNow": timeNow,
                                                             "cafeAddress": cafe.location.street_name,
                                                             })

@login_required
def ingredientAdd(request):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()


    if request.method == "GET":
        form = IngredientForm()


    elif request.method == "POST":
        form = IngredientForm(request.POST)

        if form.is_valid():
            d = form.cleaned_data
            date = d["date"]

            if date <= time_now():
                Ingredient.objects.create(
                    name=d["name"],
                    date=d["date"],
                    cafe=d["cafe"],
                    packagesAmount=d["packagesAmount"],
                    goodsAmount=d["goodsAmount"],
                    totalAmount = d["goodsAmount"] * d["packagesAmount"],
                    price=d["price"],
                )

                return HttpResponseRedirect(reverse('managerIngredientsList', args=(1,)))
            else:
                form.add_error("date", "Заданий час у майбутньому")


    return render(request, "manager/ingredient-add.html", {"form": form,
                                                           "timeNow": timeNow,
                                                           "cafeAddress": cafe.location.street_name,
                                                           "cafeID": cafe.id})

@login_required
def ingredientEdit(request, id):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()
    ingredient = get_object_or_404(Ingredient, id=id)
    originalAmount = ingredient.totalAmount
    form = None
    data = {}

    data.update({
        "name": ingredient.name,
        "date": ingredient.date,
        "sellingUnits": ingredient.name.sellingUnits,
        "cafe": cafe
    })

    if request.method == "GET":

        form = IngredientTotalAmountForm(instance=ingredient)


    elif request.method == "POST":
        form = IngredientTotalAmountForm(request.POST, instance=ingredient)
        if form.is_valid():
            d = form.cleaned_data
            if d["totalAmount"] < 0:
                form.add_error("totalAmount", "Залишок не може бути від'ємним!")
            else:
                allIngredients_ = Ingredient.objects.filter(name_id=ingredient.name_id, cafe_id=cafe.pk)
                allIngredients = []
                timeNow = datetime.datetime.now(pytz.UTC)
                # удаление просроченных ингредиентов
                for i in allIngredients_:
                    if i.date > timeNow - datetime.timedelta(days=i.name.shelfLife):
                        allIngredients.append(i)
                allIngredientsInCafe = {}
                allDishesList = [dish for dish in Dish.objects.all()]
                allDishIngredients = {} # {dish: {ingr1: amount, ingr2: amount}}
                allImportingIngredients = CouriersIngredientsTransitIngredient.objects.filter(
                    ingr_import__status__in = [s[0] for s in CouriersIngredientsTransit.STATUSES[:-1]],
                    from_cafe_id=cafe
                )

                for ingr in allIngredients:
                    cafe = ingr.cafe
                    ingr_ = ingr.name
                    amount = ingr.totalAmount

                    if ingr_ not in allIngredientsInCafe.keys():
                        allIngredientsInCafe.update({
                            ingr_: amount
                        })
                    else:
                        allIngredientsInCafe[ingr_] += amount


                for ingrTrans in allImportingIngredients:
                    ingr = ingrTrans.ingredient.name
                    amount = ingrTrans.amount

                    if ingr in allIngredientsInCafe.keys():
                        allIngredientsInCafe[ingr] -= amount


                for dish in allDishesList:
                    di = DishIngredients.objects.filter(dish_id=dish.pk)
                    if dish not in allDishIngredients.keys():
                        allDishIngredients.update({
                            dish: {}
                        })
                    for i in di:
                        allDishIngredients[dish].update({
                            i.ingredient: i.amount
                        })


                for orderPart in CafesOrderPart.objects.filter(is_done=False, cafe_id=cafe.pk):
                    for diofc in DishInOrderForCafe.objects.filter(cafes_order_part_id=orderPart.pk):
                        used = PreparedDishesUsed.objects.filter(ordered_dish_id=diofc.pk)

                        if diofc.dish:
                            for ingrName, amount in allDishIngredients[diofc.dish].items():
                                cafe = diofc.cafes_order_part.cafe
                                try:
                                    if used:
                                        allIngredientsInCafe[ingrName] -= amount * (diofc.qty - used[0].qty)
                                    else:
                                        allIngredientsInCafe[ingrName] -= amount * diofc.qty
                                except:
                                    pass

                # чтобы никакие ингредиенты не ушли в минус, и алгоритм отработал верно
                for ingr, amount in allIngredientsInCafe.items():
                    if amount < 0:
                        allIngredientsInCafe[ingr] += 0 - allIngredientsInCafe[ingr]


                for k, v in allIngredientsInCafe.items():
                    left = v - (originalAmount - d["totalAmount"])
                    if left > 0:
                        ingredient.totalAmount = d["totalAmount"]
                        ingredient.save()
                        return HttpResponseRedirect(reverse('managerIngredientsList', args=(1,)))
                    else:
                        form.add_error("totalAmount", "Використовується більше інгредієнтів цього типу, ніж залишається")


    return render(request, "manager/ingredient-edit.html", {"form": form,
                                                            "data": data,
                                                            "timeNow": timeNow,
                                                            "cafeAddress": cafe.location.street_name,
                                                            "cafeID": cafe.id})

@login_required
def ingredientNameAdd(request):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    if request.method == "GET":
        form = IngredientNameForm()

    elif request.method == "POST":
        form = IngredientNameForm(request.POST)

        if form.is_valid():
            d = form.cleaned_data

            created = IngredientName.objects.create(
                name=d["name"],
                shelfLife=d["shelfLife"],
                buyingUnits=d["buyingUnits"],
                sellingUnits=d["sellingUnits"],
            )

            return HttpResponse('''
            <script type=\"application/javascript\">
                var option = window.opener.document.createElement("option");
                option.value = '%(id)s';
                option.innerHTML = '%(name)s';
                select = window.opener.document.getElementById("name");
                select.appendChild(option);

                // убираем аттрибут selected у элементов списка
                var elements = window.opener.document.getElementById("name").options;
                for(var i = 0; i < elements.length; i++){
                  elements[i].selected = false;
                }
                option.selected = true;
                window.opener.changeUnits(%(id)s);
                window.close()
            </script>''' % {"id": created.id,
                            "name": created.name})

    return render(request, "manager/ingredientName-add.html", {"form": form,
                                                               "timeNow": timeNow,
                                                               "cafeAddress": cafe.location.street_name,
                                                               "cafeID": cafe.id})


@login_required
def ingredientNameEdit(request, id):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()
    ingredientName = get_object_or_404(IngredientName, pk=id)

    if request.method == "GET":
        form = IngredientNameForm(instance=ingredientName)

    elif request.method == "POST":
        form = IngredientNameForm(request.POST, instance=ingredientName)

        if form.is_valid():
            ingrName = form.save()

            return HttpResponse('''
            <script type=\"application/javascript\">
                select = window.opener.document.getElementById("name");
                window.opener.changeUnits(%(id)s);
                select.querySelector('option[value="%(id)s"]').innerText = '%(name)s';
                window.close()
            </script>''' % {"id": ingrName.id,
                            "name": ingrName.name})

    return render(request, "manager/ingredientName-add.html", {"form": form,
                                                               "timeNow": timeNow,
                                                               "cafeAddress": cafe.location.street_name,
                                                               "cafeID": cafe.id})


class getIngredientNameUnits(APIView):
    def get(self, request, id):
        try:
            toSearchInParent = 0
            try:
                p = int(request.GET["parent"])
                if p == 1: toSearchInParent = 1
            except: pass

            id = int(id)
            if not toSearchInParent:
                ingredient = IngredientName.objects.get(id=id)
            else:
                ingredient = Ingredient.objects.get(id=id).name
        except:
            raise APIException()

        return HttpResponse(json.dumps({
            "buyingUnits": ingredient.buyingUnits.name,
            "sellingUnits": ingredient.sellingUnits.name
        }))


@login_required
def dishesList(request, page):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    dishesPerPage = 20

    dishes = Dish.objects.all().order_by("-id")
    pagination = Paginator(dishes, dishesPerPage)

    dishes = pagination.get_page(page)

    return render(request, "manager/dishes-list.html", {"dishes": dishes,
                                                        "timeNow": timeNow,
                                                        "cafeAddress": cafe.location.street_name})


@login_required
def dishAdd(request):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    if request.method == "GET":
        dish = DishForm()
        formset = DishFormSet()

    elif request.method == "POST":
        dish = DishForm(request.POST, request.FILES)
        print("request.POST", request.POST)
        formset = DishFormSet(request.POST)

        is_error = False
        if not formset.is_valid():
            print('notv1')
        if not dish.is_valid():
            is_error = True
            print('notv2')

        print('1')
        if not is_error:
            f = formset.cleaned_data
            d = dish.cleaned_data

            createdDish = Dish.objects.create(
                name=d["name"],
                image=d["image"],
                description=d["description"],
                calorie=d["calorie"],
                weight=d["weight"],
                cooking_time=d["cooking_time"],
                prepared_cooking_time=d["prepared_cooking_time"],
                prepared_lifetime=d["prepared_lifetime"],
                price=d["price"],
            )

            for ingredient in f:
                if "ingredient" in ingredient.keys():
                    DishIngredients.objects.create(
                        dish=createdDish,
                        ingredient=ingredient["ingredient"],
                        amount=ingredient["amount"],
                    )

            return HttpResponseRedirect(reverse('managerDishesList', args=(1,)))


    return render(request, "manager/dish-add.html", {"dish": dish,
                                                     "formset": formset,
                                                     "timeNow": timeNow,
                                                     "mode": "add",
                                                     "cafeAddress": cafe.location.street_name,
                                                     "cafeID": cafe.id})


@login_required
def dishEdit(request, id):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    dish = get_object_or_404(Dish, pk=id)
    dishIngredients = DishIngredients.objects.filter(dish_id=id)
    dishInstance = Dish.objects.get(pk=id)

    if request.method == "GET":
        dish = DishForm(instance=dish)
        formset = DishFormSet(instance=dishInstance)

    elif request.method == "POST":
        dish = DishForm(request.POST, request.FILES, instance=dish)
        formset = DishFormSet(request.POST)

        if dish.is_valid():
            created_dish = dish.save(commit=False)
            formset = DishFormSet(request.POST, instance=created_dish)

            if formset.is_valid():
                created_dish.save()
                formset.save()

                return HttpResponseRedirect(reverse('managerDishesList', args=(1,)))


    return render(request, "manager/dish-add.html", {"dish": dish,
                                                     "formset": formset,
                                                     "timeNow": timeNow,
                                                     "mode": "edit",
                                                     "cafeAddress": cafe.location.street_name,
                                                     "cafeID": cafe.id})


@login_required
def ingrTransitList(request, page):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    objsPerPage = 20

    ingrTransIngrFrom = CouriersIngredientsTransitIngredient.objects.filter(from_cafe_id=cafe.pk).order_by("-id")
    ingrTransTo = CouriersIngredientsTransit.objects.filter(to_cafe_id=cafe.pk).order_by("-id")

    dict_ = {}

    for i in ingrTransIngrFrom:
        ingrTrans = i.ingr_import

        key = ("from", ingrTrans, i.status)
        if key not in dict_.keys():
            dict_.update({key: {}})

        dict_[key].update({
            i.ingredient: i.amount
        })


    for i in ingrTransTo:
        key = ("to", i, i.status)
        if key not in dict_.keys():
            dict_.update({key: {}})

        all_ = CouriersIngredientsTransitIngredient.objects.filter(ingr_import_id = i.pk)

        for ingredient in all_:
            dict_[key].update({
                ingredient.ingredient: ingredient.amount
            })


    tuple_ = ()
    for (mode, obj, status), ingr in dict_.items():
        ingrs = ()
        for i, amount in ingr.items():
            ingrs += ((i, amount),)

        tuple_ += (((mode, obj, status), ingrs),)


    pagination = Paginator(tuple_, objsPerPage)
    ingrTransits = pagination.get_page(page)

    return render(request, "manager/ingrTransit-list.html", {"ingrTransits": ingrTransits,
                                                             "timeNow": timeNow,
                                                             "cafeAddress": cafe.location.street_name})


@login_required
def ingrTransitAdd(request):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    if request.method == "GET":
        ingrTransit = IngrTransitForm()
        form = IngrTransitIngrForm()
        #form.fields["ingredient"].queryset = Ingredient.objects.filter(is_overdued=False, cafe_id=cafe.pk)
        formset = IngrTransitIngrFormSet_() #cafe.pk


    elif request.method == "POST":
            ingrTransit = IngrTransitForm(request.POST)
            formset = IngrTransitIngrFormSet_(request.POST) #cafe.pk,
            if ingrTransit.is_valid():
                is_error = False

                d = {k: v for k, v in ingrTransit.cleaned_data.items()}
                from_cafe = d["from_cafe"]
                to_cafe   = d["to_cafe"]

                print(from_cafe)
                print(to_cafe)
                if from_cafe == to_cafe:
                    is_error = True
                    ingrTransit.add_error("to_cafe", "Відправник і одержувач не може бути однаковий")
                elif cafe not in [from_cafe, to_cafe]:
                    is_error = True
                    ingrTransit.add_error("from_cafe", "Одне з обраних кафе повинно бути Вашим")

                if not is_error:

                    if formset.is_valid():

                        allIngredients_ = Ingredient.objects.filter(cafe_id=from_cafe.pk)
                        allIngredients = []
                        timeNow = datetime.datetime.now(pytz.UTC)
                        # удаление просроченных ингредиентов
                        for i in allIngredients_:
                            if i.date > timeNow - datetime.timedelta(days=i.name.shelfLife):
                                allIngredients.append(i)

                        allIngredientsNames = {} # {ingrName: amount}
                        for ingredient in allIngredients:
                            ingrName = ingredient.name_id
                            if ingrName not in allIngredientsNames.keys():
                                allIngredientsNames.update({ingrName: 0})

                            allIngredientsNames[ingrName] += ingredient.totalAmount

                        for orderPart in CafesOrderPart.objects.filter(is_done=False):
                            for diofc in DishInOrderForCafe.objects.filter(cafes_order_part_id=orderPart.pk):
                                used = PreparedDishesUsed.objects.filter(ordered_dish_id=diofc.pk)
                                if diofc.dish:
                                    for di in DishIngredients.objects.filter(dish_id=diofc.dish.pk):
                                        ingrName = di.ingredient_id
                                        amount = di.amount
                                        try:
                                            if used:
                                                allIngredientsNames[ingrName] -= amount * (
                                                            diofc.qty - used[0].qty)
                                            else:
                                                allIngredientsNames[ingrName] -= amount * diofc.qty
                                        except:
                                            pass

                        print(allIngredientsNames)
                        fs = formset.cleaned_data

                        statuses = []
                        for formID, f in enumerate(fs):
                            print(f.keys())
                            if f and "ingredient_" in f.keys() and "amount_" in f.keys():
                                print(f)
                                ingredient_id = f["ingredient_"].pk
                                amount = f["amount_"]

                                if ingredient_id in allIngredientsNames.keys() and allIngredientsNames[ingredient_id] >= amount:
                                    statuses.append(True)
                                else:
                                    statuses.append(False)
                                    formset[formID].add_error("ingredient_", "Немає в наявності в таких кількостях")


                        if statuses and False not in statuses:
                            cit = CouriersIngredientsTransit()
                            cit.to_cafe = to_cafe
                            cit.save()

                            for f in fs:
                                if f and "ingredient_" in f.keys() and "amount_" in f.keys():
                                    citi = CouriersIngredientsTransitIngredient()
                                    citi.ingr_import = cit
                                    citi.from_cafe = from_cafe
                                    citi.ingredient_id = f["ingredient_"].pk
                                    citi.amount = f["amount_"]
                                    citi.save()

                            return HttpResponseRedirect(reverse('managerIngrTransitList', args=(1,)))


    return render(request, "manager/ingrTransit-add.html", {"ingrTransit": ingrTransit,
                                                            "formset": formset,
                                                            "timeNow": timeNow,
                                                            "mode": "add",
                                                            "cafeAddress": cafe.location.street_name,
                                                            "cafeID": cafe.id})


@login_required
def ingrTransitSubmit(request, id):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    cit = get_object_or_404(CouriersIngredientsTransit, pk=id)
    citi = CouriersIngredientsTransitIngredient.objects.filter(ingr_import_id=cit.pk, from_cafe_id=cafe.pk)
    mode = ""
    to_cafe = cit.to_cafe.location.street_name
    if cafe.pk == cit.to_cafe_id:
        mode = "to"
    elif citi:
        mode = "from"
        citi = citi[0]
    else:
        return Http404

    formset = None
    needed_ingrs = {}
    is_done = False
    sent_ingrs = {}
    errors = []

    #from_cafe = CouriersIngredientsTransitIngredient.objects.filter(ingr_import_id=cit.pk, )

    if mode == "from":
        needed_ingrs = CouriersIngredientsTransitIngredient.objects.filter(ingr_import_id=cit.pk, from_cafe_id=cafe.pk)

    elif mode == "to":
        needed_ingrs = CouriersIngredientsTransitIngredient.objects.filter(ingr_import_id=cit.pk)


    if request.method == "GET":

        if mode == "from":
            if citi.status != "done":
                is_done = False
                formset = IngrTransitIngrSubmitFormSet(cafe.pk)
            else:
                is_done = True
                sent_ingrs.update({i.ingredient: i.amount for i in IngredientsInTransit.objects.filter(ingr_import_ingr_id = citi.pk)})

        elif mode == "to":
            if cit.status != "done":
                is_done = False

            else:
                is_done = True


    elif request.method == "POST":

            if mode == "from":
                if citi.status != "done":
                    formset = IngrTransitIngrSubmitFormSet(cafe.pk, request.POST)

                    if formset.is_valid():

                        for i in formset.cleaned_data:
                            print(i)

                        needed_ingrs_ = {i.ingredient: i.amount for i in copy.deepcopy(needed_ingrs)}
                        allIngredients_ = Ingredient.objects.filter(cafe_id=cafe.pk)
                        allIngredients = []
                        timeNow = datetime.datetime.now(pytz.UTC)
                        # удаление просроченных ингредиентов
                        for i in allIngredients_:
                            if i.date > timeNow - datetime.timedelta(days=i.name.shelfLife):
                                allIngredients.append(i)
                        allIngredientsInCafe = {}
                        allDishesList = [dish for dish in Dish.objects.filter(is_active=True)]
                        allDishIngredients = {} # {dish: {ingr1: amount, ingr2: amount}}
                        allImportingIngredients = CouriersIngredientsTransitIngredient.objects.filter(
                            ingr_import__status__in = [s[0] for s in CouriersIngredientsTransit.STATUSES[:-1]],
                            from_cafe_id=cafe
                        )

                        for ingredient in allIngredients:
                            cafe = ingredient.cafe
                            ingr_ = ingredient.name
                            amount = ingredient.totalAmount

                            for formID, i in enumerate(formset):
                                i = i.cleaned_data
                                if "ingredient_" in i.keys():
                                    ingr = i["ingredient_"]
                                    if ingredient == ingr:
                                        if i["amount_"] >= amount:
                                            formset[formID].add_error("ingredient_", "На складі не вистачає інгредієнтів")
                                        break

                            if ingr_ not in allIngredientsInCafe.keys():
                                allIngredientsInCafe.update({
                                    ingr_: amount
                                })
                            else:
                                allIngredientsInCafe[ingr_] += amount


                        for ingrTrans in allImportingIngredients:
                            ingr = ingrTrans.ingredient.name
                            amount = ingrTrans.amount

                            if ingr not in allIngredientsInCafe.keys():
                                allIngredientsInCafe.update({
                                    ingr: 0
                                })
                            allIngredientsInCafe[ingr] -= amount


                        for dish in allDishesList:
                            di = DishIngredients.objects.filter(dish_id=dish.pk)
                            if dish not in allDishIngredients.keys():
                                allDishIngredients.update({
                                    dish: {}
                                })
                            for i in di:
                                allDishIngredients[dish].update({
                                    i.ingredient: i.amount
                                })


                        for orderPart in CafesOrderPart.objects.filter(is_done=False, cafe_id=cafe.pk):
                            for diofc in DishInOrderForCafe.objects.filter(cafes_order_part_id=orderPart.pk):
                                used = PreparedDishesUsed.objects.filter(ordered_dish_id=diofc.pk)

                                if diofc.dish:
                                    for ingrName, amount in allDishIngredients[diofc.dish].items():
                                        cafe = diofc.cafes_order_part.cafe
                                        try:
                                            if used:
                                                allIngredientsInCafe[ingrName] -= amount * (diofc.qty - used[0].qty)
                                            else:
                                                allIngredientsInCafe[ingrName] -= amount * diofc.qty
                                        except:
                                            pass

                        # чтобы никакие ингредиенты не ушли в минус, и алгоритм отработал верно
                        for ingr, amount in allIngredientsInCafe.items():
                            if amount < 0:
                                allIngredientsInCafe[ingr] += 0 - allIngredientsInCafe[ingr]


                        for k, v in allIngredientsInCafe.items():
                            print(k, ":", v)


                        for i in formset:
                            i = i.cleaned_data
                            if "ingredient_" in i.keys():
                                ingr = i["ingredient_"]
                                amount = i["amount_"]
                                if ingr:
                                    ingrName = ingr.name
                                    if ingrName not in needed_ingrs_.keys():
                                        needed_ingrs_.update({ingrName: 0})
                                    needed_ingrs_[ingrName] -= amount

                                    allIngredientsInCafe[ingrName] -= amount

                        error = False

                        for ingr, amount in allIngredientsInCafe.items():
                            if amount < 0:
                                error = True
                                for formID, i in enumerate(formset):
                                    i = i.cleaned_data
                                    ingrName = i["ingredient_"].name
                                    if ingrName == ingr:
                                        formset[formID].add_error("ingredient_", "На складі не вистачає таких інгредієнтів")
                                        break

                        for ingr, ingrsLeft in needed_ingrs_.items():
                            if ingrsLeft > 0: # если дано ингредиентов меньше, чем нужно
                                error = True
                                for formID, i in enumerate(formset):
                                    i = i.cleaned_data
                                    if "ingredient_" in i.keys():
                                        ingrName = i["ingredient_"].name
                                        if ingrName == ingr:
                                            formset[formID].add_error("ingredient_", "Задано менше необхідного")
                                            break

                        if not error:
                            for i in formset:
                                i = i.cleaned_data
                                if "ingredient_" in i.keys() and i["ingredient_"]:
                                    ingredient = i["ingredient_"]
                                    amount = i["amount_"]
                                    IngredientsInTransit.objects.create(
                                        ingr_import_id = cit.pk,
                                        ingr_import_ingr_id = citi.pk,
                                        ingredient_id = ingredient.pk,
                                        amount = amount
                                    )

                                    ingredient.totalAmount -= amount
                                    ingredient.save()

                            citi.status = "done"
                            citi.save()
                            return HttpResponseRedirect(reverse('managerIngrTransitList', args=(1,)))

            elif mode == "to":
                citis = CouriersIngredientsTransitIngredient.objects.filter(ingr_import_id=cit.pk)
                for i in citis:
                    print()
                    print(i)
                    print(i.status)
                    if i.status != "done":
                        errors.append("Не всі кафе підтвердили передачу інгредієнтів!")
                        break

                if not errors:
                    ingrs = IngredientsInTransit.objects.filter(ingr_import_id=cit.pk)
                    for ingr in ingrs:
                        ingredient = ingr.ingredient
                        amount = ingr.amount

                        Ingredient.objects.create(
                            name_id = ingredient.name_id,
                            date = ingredient.date,
                            cafe_id = cafe.pk,
                            packagesAmount = amount/ingredient.goodsAmount,
                            goodsAmount = ingredient.goodsAmount,
                            totalAmount = amount,
                            is_overdued = False,
                            price = ingredient.price
                        )
                    cit.status = "done"
                    cit.save()
                    return HttpResponseRedirect(reverse('managerIngrTransitList', args=(1,)))



    return render(request, "manager/ingrTransit-submit.html", {"to_cafe": to_cafe,
                                                               #"from_cafe": from_cafe,
                                                               "is_done": is_done,
                                                               "sent_ingrs": sent_ingrs,
                                                               "needed_ingrs": needed_ingrs,
                                                               "formset": formset,
                                                               "mode": mode,
                                                               "errors": errors,
                                                               "timeNow": timeNow,
                                                               "cafeAddress": cafe.location.street_name,
                                                               "cafeID": cafe.id})


@login_required
def statistics(request):
    user = request.user
    userProfile = Profile.objects.get(user_id = user.pk)

    if not userProfile.worksInCafe:
        return HttpResponseRedirect("/")

    cafe = userProfile.worksInCafe
    timeNow = time_now()

    print(re.match(r"\d{4}-{1}(\d{2})-{1}(\d{2})", "2018-10-17").group(0))

    return render(request, "manager/statistics.html", {
                                                       "timeNow": timeNow,
                                                       "cafeAddress": cafe.location.street_name,
                                                       "cafeID": cafe.id})