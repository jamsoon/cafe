from . import views
from statistics_ import views as statisticsViews

from django.conf import settings
from django.urls import path, re_path, include
from django.conf.urls import *


urlpatterns = [
    url(r"^$", views.home, name="managerHome"),
    path("order/list/<int:page>/", views.ordersList, name="managerOrdersList"),
    path("order/details/<int:id>/", views.orderDetails, name="managerOrderDetails"),

    path("prepared-dish/list/<int:page>/", views.preparedDishesList, name="managerPreparedDishesList"),
    path("prepared-dish/details/<int:id>/", views.preparedDishDetails, name="managerPreparedDishDetails"),
    path("prepared-dish/add/", views.preparedDishAdd, name="managerPreparedDishAdd"),

    path("ingredient/list/<int:page>/", views.ingredientsList, name="managerIngredientsList"),
    path("ingredient/add/", views.ingredientAdd, name="managerIngredientAdd"),
    path("ingredient/edit/<int:id>/", views.ingredientEdit, name="managerIngredientEdit"),

    path("ingredient-name/add/", views.ingredientNameAdd, name="managerIngredientNameAdd"),
    path("ingredient-name/edit/<int:id>/", views.ingredientNameEdit, name="managerIngredientNameEdit"),
    path("ingredient-name/get-units/<int:id>/", views.getIngredientNameUnits.as_view(), name="managerIngredientNameGetUnits"),

    path("statistics/", views.statistics, name="managerStatistics"),

    path("dish/list/<int:page>/", views.dishesList, name="managerDishesList"),
    path("dish/add/", views.dishAdd, name="managerDishAdd"),
    path("dish/edit/<int:id>/", views.dishEdit, name="managerDishEdit"),

    path("ingredient-transit/list/<int:page>/", views.ingrTransitList, name="managerIngrTransitList"),
    path("ingredient-transit/add/", views.ingrTransitAdd, name="managerIngrTransitAdd"),
    path("ingredient-transit/submit/<int:id>/", views.ingrTransitSubmit, name="managerIngrTransitSubmit"),
    # path("dish/edit/<int:id>/", views.ingredientEdit, name="managerIngredientEdit"),
]