from django.shortcuts import render
from django.contrib.auth.models import User
from authentication.serializers import UserSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from authentication.serializers import TokenCreateSerializer
import djoser
from djoser.conf import settings
from djoser import utils
from rest_framework import generics, permissions, status, views, viewsets
from rest_framework.authtoken.models import Token

from dishes.models import Dish
from authentication.permissions import IsCourier
from _auth.models import Profile

class UserListAPIView(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


# def token_request(request):
#     print(request)
#     if Token.user_requested_token() and Token.token_request_is_warranted():
#         token = Token.objects.get(user=request.user)
#         if not Token.objects.get(user=request.user):
#             token = Token.objects.create(user=request.user)
#
#         return Response({"token": token.key})


# class CustomTokenCreateView(utils.ActionViewMixin, generics.GenericAPIView):
#     """
#     Use this endpoint to obtain user authentication token.
#     """
#     serializer_class = TokenCreateSerializer
#     permission_classes = [permissions.AllowAny]
#
#     def _action(self, serializer):
#         user = User.objects.get(username=serializer.data["username"])
#
#         token = utils.login_user(self.request, serializer.user)
#         token_serializer_class = settings.SERIALIZERS.token
#         content = {
#             'Token': token_serializer_class(token).data["auth_token"],
#             'promptmsg': 'Водила привет!',
#             'status': '200'
#         }
#         return Response(
#             data=content,
#             status=status.HTTP_200_OK,
#         )