from rest_framework import permissions
from django.contrib.auth.models import User
from _auth.models import Profile
from dishes.models import CouriersOrder, CouriersIngredientsTransit

class IsCourier(permissions.BasePermission):
    message = "Ви не є кур'єром!"

    def has_permission(self, request, view):
        user = request.user
        print(user)
        try:
            role = Profile.objects.get(user=user).role

            try:
                if role and role == "courier":
                    return True
                else:
                    return False

            except AttributeError:
                return False

        except TypeError:
            return False

class IsCourierFree(permissions.BasePermission):
    message = "У Вас вже є замовлення!"

    def has_permission(self, request, view):
        user = request.user
        statuses = [st[0] for st in CouriersOrder.STATUSES]

        cour_o = CouriersOrder.objects.filter(courier_id=user.pk, status__in=statuses[:-1])
        cour_it = CouriersIngredientsTransit.objects.filter(courier_id=user.pk, status__in=statuses[:-1])

        if cour_o or cour_it:
            return False
        else:
            return True