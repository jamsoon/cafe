var pastWidth;
var openMenu = false;

$(document).ready(function() {
	pastWidth = $(window).width();
	$('.open-menu').click(function() {
		if (!openMenu) {
			funcOpenMenu();
		}
		else {
			funcCloseMenu();
		}
	});

	$('.overflow').click(function() {
		if (openMenu) {
			funcCloseMenu();
		}
	});
	$(window).resize(function() {
		if (openMenu && pastWidth != $(window).width()) {
			funcCloseMenu();
			pastWidth = $(window).width();
		}
	});
});

function funcOpenMenu() {
	openMenu = true;
	$('.mobile-menu').css({
		'top':'0px'
	});
	$('.open-menu div:nth-of-type(1)').css({
		'top':'38.5px',
		'transform':'rotate(45deg)'
	});
	$('.open-menu div:nth-of-type(2)').css({
		'bottom':'38.5px',
		'transform':'rotate(-45deg)'
	});
	$('.open-menu div:nth-of-type(3)').css({
		'opacity':'0'
	});
	$('.overflow').css({
		'display':'block'
	});
	setTimeout(function() {
		$('.overflow').css({
			'opacity':'1'
		});
	});
}

function funcCloseMenu() {
	openMenu = false;
	$('.mobile-menu').css({
		'top':'-360px'
	});
	$('.open-menu div:nth-of-type(1)').css({
		'top':'20px',
		'transform':'rotate(0deg)'
	});
	$('.open-menu div:nth-of-type(2)').css({
		'bottom':'20px',
		'transform':'rotate(0deg)'
	});
	$('.open-menu div:nth-of-type(3)').css({
		'opacity':'1'
	});
	$('.overflow').css({
		'display':'none',
		'opacity':'0'
	});
}