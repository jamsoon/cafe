$(document).ready(function() {
	var open = false;
	$('.button-category').click(function() {
		if (open == false) {
			open = true;
			$('.categories').css({
				'max-height':'400px',
			});
		}
		else {
			open = false;
			$('.categories').css({
				'max-height':'0px'
			});

		}
	});
});