var off;
var offTeam;
var activeTeam = false;
var openMenu = false;
var pastWidth;
$(document).ready(funcHeader());
$(document).ready(function() {
	pastWidth = $(window).width();
	$('.menu > div font div a').click(function() {
		var classs = $(this).attr('name');
		var top = $(classs).offset().top - 100;
		$('body,html').animate({scrollTop: top}, 750);
	});
	$('.mobile-menu > div a').click(function() {
		var classs = $(this).attr('name');
		var top = $(classs).offset().top - 100;
		$('body,html').animate({scrollTop: top}, 750);
	});
	$('.href-to-footer').click(function() {
		var classs = $(this).attr('name');
		var top = $(classs).offset().top - 100;
		$('body,html').animate({scrollTop: top}, 750);
	});

	$('.open-menu').click(function() {
		if (!openMenu) {
			funcOpenMenu();
		}
		else {
			funcCloseMenu();
		}
	});

	$('.overflow').click(function() {
		if (openMenu) {
			funcCloseMenu();
		}
	});

	//off = $('.article-header:nth-of-type(1)').offset().top;
	offTeam = $('.our-team').offset().top;
	funcHeader();
	$(window).scroll(function() {
		funcHeader();
	});
	$(window).resize(function() {
		//off = $('.article-header:nth-of-type(1)').offset().top;
		if (openMenu && pastWidth != $(window).width()) {
			funcCloseMenu();
			pastWidth = $(window).width();
		}
		offTeam = $('.our-team').offset().top;
		funcHeader();
	});

});

function funcOpenMenu() {
	openMenu = true;
	$('.mobile-menu').css({
		'top':'0px'
	});
	$('.open-menu div:nth-of-type(1)').css({
		'top':'38.5px',
		'transform':'rotate(45deg)'
	});
	$('.open-menu div:nth-of-type(2)').css({
		'bottom':'38.5px',
		'transform':'rotate(-45deg)'
	});
	$('.open-menu div:nth-of-type(3)').css({
		'opacity':'0'
	});
	$('.overflow').css({
		'display':'block'
	});
	setTimeout(function() {
		$('.overflow').css({
			'opacity':'1'
		});
	});
}

function funcCloseMenu() {
	openMenu = false;
	$('.mobile-menu').css({
		'top':'-360px'
	});
	$('.open-menu div:nth-of-type(1)').css({
		'top':'20px',
		'transform':'rotate(0deg)'
	});
	$('.open-menu div:nth-of-type(2)').css({
		'bottom':'20px',
		'transform':'rotate(0deg)'
	});
	$('.open-menu div:nth-of-type(3)').css({
		'opacity':'1'
	});
	$('.overflow').css({
		'display':'none',
		'opacity':'0'
	});
}

var more = true;
function funcHeader() {
	var scrollTop = $(window).scrollTop();
	if ($(window).width() > 1080) {
		if (scrollTop > 0) {
			$('.menu').css({
				'top':'0px',
				'transition':'300ms'
			});
			$('.top-menu').css({
				'top':'-40px',
				'transition':'300ms'
			});
			$('.start').css({
				'bottom':'-100px'
			});
		}
		else {
			$('.menu').css({
				'top':'40px',
				'transition':'300ms'
			});
			$('.top-menu').css({
				'top':'0px',
				'transition':'300ms'
			});
			$('.start').css({
				'bottom':'15px'
			});
		}

		if (scrollTop > 250) {

			$('.menu').css({
				'background':'white',
				'box-shadow':'0px 3px 3px rgba(0, 0, 0, 0.5)',
				'transition':'300ms'
			});

			$('header').css({
				'top':scrollTop / 2 + 60 + 'px',
				'height':'calc(100% - 60px)'
			});
			if (more == false) {
				more = true;
				$('header').css({
					'transition':'150ms'
				});
				setTimeout(function() {
					$('header').css({
						'transition':'1ms'
					});
				}, 150);
			}
			$('.menu > div > a').css({
				'color':'black'
			});
			$('.menu font').css({
				'color':'black'
			});
		}
		else {

			$('.menu').css({
				'background':'rgba(0, 0, 0, 0)',
				'box-shadow':'none',
				'transition':'300ms'

			});

			$('header').css({
				'top':scrollTop / 2 + 'px',
				'height':'100%'
			});
			$('.menu > div > a').css({
				'color':'white'
			});
			$('.menu font').css({
				'color':'white'
			});
			if (more == true) {
				more = false;
				$('header').css({
					'transition':'150ms'
				});
				setTimeout(function() {
					$('header').css({
						'transition':'1ms'
					});
				}, 150);
			}
		}
	}
	else {
		if (scrollTop > 0) {
			$('.top-menu').css({
				'top':'-60px',
				'transition':'300ms'
			});
			$('.start').css({
				'bottom':'-130px'
			});
		}
		else {
			$('.top-menu').css({
				'top':'0px',
				'transition':'300ms'
			});
			$('.start').css({
				'bottom':'15px'
			});
		}
		$('header').css({
			'top':'80px',
			'height':'calc(100% - 80px)'
		});
	}
	/*var bg1Top = scrollTop + $(window).height() / 2 - off;
	$('.article-header:nth-of-type(1)').css({
		'background-position':'0px calc(50% + 80px - ' + bg1Top / 4 + 'px)'
	});*/

	if (scrollTop + $(window).height() - 250 > offTeam && !activeTeam) {
		activeTeam = true;
		$('.our-team div:nth-of-type(1)').css({
			'transform':'scale(1.15)',
			'opacity': 1,
			'transition':'1000ms'
		});
		setTimeout(function() {
			$('.our-team div:nth-of-type(1)').css({
				'transform':'scale(1)',
				'transition':'200ms'
			});
		}, 900);
		setTimeout(function() {
			$('.our-team div:nth-of-type(2)').css({
				'transform':'scale(1.15)',
				'opacity': 1,
				'transition':'1000ms'
			});
		}, 300);
		setTimeout(function() {
			$('.our-team div:nth-of-type(2)').css({
				'transform':'scale(1)',
				'transition':'200ms'
			});
		}, 1200);
		setTimeout(function() {
			$('.our-team div:nth-of-type(3)').css({
				'transform':'scale(1.15)',
				'opacity': 1,
				'transition':'1000ms'
			});
		}, 600);
		setTimeout(function() {
			$('.our-team div:nth-of-type(3)').css({
				'transform':'scale(1)',
				'transition':'200ms'
			});
		}, 1500);
		setTimeout(function() {
			$('.our-team div:nth-of-type(4)').css({
				'transform':'scale(1.15)',
				'opacity': 1,
				'transition':'1000ms'
			});
		}, 900);
		setTimeout(function() {
			$('.our-team div:nth-of-type(4)').css({
				'transform':'scale(1)',
				'transition':'200ms'
			});
		}, 1800);
		setTimeout(function() {
			$('.our-team div:nth-of-type(5)').css({
				'transform':'scale(1.15)',
				'opacity': 1,
				'transition':'1000ms'
			});
		}, 1200);
		setTimeout(function() {
			$('.our-team div:nth-of-type(5)').css({
				'transform':'scale(1)',
				'transition':'200ms'
			});
		}, 2100);
	}
	else if (scrollTop + $(window).height() - 250 <= offTeam && activeTeam){
		activeTeam = false;
		$('.our-team div').css({
			'transform':'scale(0.1)',
			'opacity': 0,
			'transition':'500ms'
		});
	}
}