var inMove = false;
$(document).ready(function() {

	$('.open-courier').click(function() {
		funcOpenModal('#courier');
	});

	$('.overlay').click(function() {
		funcCloseModal();
	});
	$('.close-modal').click(function() {
		funcCloseModal();
	});

});

function funcCloseModal() {
	if (!inMove) {
		$('.modal-window').css({
			'opacity':'0',
			'transform':'scale(0.1)'
		});
		$('.overlay').css({
			'opacity':'0'
		});
		$('.close-modal').css({
			'opacity':'0',
		});
		$('body').css({
			'overflow':'auto'
		});
		setTimeout(function() {
			$('.modal-window').css({
				'display':'none'
			});
			$('.overlay').css({
				'display':'none'
			});
			$('.close-modal').css({
				'display':'none',
			});
		}, 1000);
	}
}

function funcOpenModal(modal) {
	if (!inMove) {
		inMove = true;
		$(modal).css({
			'display':'block'
		});
		$('.overlay').css({
			'display':'block'
		});
		$('.close-modal').css({
			'display':'block'
		});
		$('body').css({
			'overflow':'hidden'
		});
		setTimeout(function() {
			$(modal).css({
				'opacity':'1',
				'transform':'scale(1)'
			});
			$('.overlay').css({
				'opacity':'0.6',
			});
			$('.close-modal').css({
				'opacity':'1',
			});
		}, 2);
		setTimeout(function() {
			inMove = false;
		}, 1000);
	}
}