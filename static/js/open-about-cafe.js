$(document).ready(function() {
	
	var activeAboutCafe = false;
	$('.href-about-cafe font').click(function() {
		if (!activeAboutCafe) {
			activeAboutCafe = true;
			$('.href-about-cafe div').css({
				'height':'auto',
				'padding':'5px'
			});
		}
		else {
			activeAboutCafe = false;
			$('.href-about-cafe div').css({
				'height':'0px',
				'padding':'0px 5px'
			});
		}
	});
	
});