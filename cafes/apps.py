from django.apps import AppConfig


class CafesConfig(AppConfig):
    name = 'cafes'
    verbose_name = 'Мережа кафе'
