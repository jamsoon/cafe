from django.db import models
from locations.models import Location

class Cafe(models.Model):
    location = models.ForeignKey(Location, null=True, blank=True, on_delete=models.SET(None))
    is_active = models.BooleanField("Чи активне", default=True)
    # street = models.CharField("Вулиця", max_length=200)
    # coordinates = models.CharField("Координаты", max_length=200, unique=True)
    workers = models.IntegerField("Працівники", default=1)

    def __str__(self):
        return self.location.street_name if self.location and self.location.street_name else "Кафе %d" % self.id

    class Meta:
        verbose_name = "Кафе"
        verbose_name_plural = "Місця розташування кафе"