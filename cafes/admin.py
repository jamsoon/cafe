from django.contrib import admin
from cafes.models import *

class CafeAdm(admin.ModelAdmin):
    ordering = ("location",)
    list_display = ("location", "workers",)

admin.site.register(Cafe, CafeAdm)