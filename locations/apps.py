from django.apps import AppConfig


class LocationsConfig(AppConfig):
    name = 'locations'
    verbose_name = "Локація"
    verbose_name_plural = "Локації"
