from django.db import models
from django.contrib.auth.models import User

class Location(models.Model):
    street_name = models.CharField("Вулиця", max_length=255, blank=True, null=True)
    home_details = models.CharField("Деталі житла", max_length=255, blank=True, null=True)
    lat = models.FloatField("Широта", blank=True, null=True)
    lon = models.FloatField("Довгота", blank=True, null=True)

    def __str__(self):
        return self.street_name or ""

    class Meta:
        verbose_name = "Локація"
        verbose_name_plural = "Локації"

class LastPosition(models.Model):
    user = models.OneToOneField(User, verbose_name="Корстувач", on_delete=models.CASCADE)
    lat = models.FloatField("Широта", blank=True, null=True)
    lon = models.FloatField("Довгота", blank=True, null=True)

    def __str__(self):
        return "%d" % self.id

    class Meta:
        verbose_name = "остання позиція"
        verbose_name_plural = "останні позиції"