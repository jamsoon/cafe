from django.contrib import admin
from .models import *

class LocationAdm(admin.ModelAdmin):
    list_display = ("street_name", )



admin.site.register(Location, LocationAdm)
admin.site.register(LastPosition)